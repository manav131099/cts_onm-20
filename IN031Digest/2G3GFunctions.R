constYieldVal = c(445.2,318)

timetomins = function(x)
{
	lists = unlist(strsplit(x,"\\ "))
	seq1 = seq(from = 2, to = length(lists),by=2)
	lists = lists[seq1]
	lists = unlist(strsplit(lists,":"))
	seq1 = seq(from = 1, to = length(lists),by = 2)
	seq2 = seq(from = 2, to = length(lists),by = 2)
	hr = as.numeric(lists[seq1])
	min = as.numeric(lists[seq2])
	return((hr * 60)+min+1)
}
secondGenData = function(path,pathw,mtno)
{
	dataread = read.table(path,header = T,sep = "\t")
	{
	if(nrow(dataread) < 1)
	{
		day = unlist(strsplit(path,"/"))
		day = day[length(day)]
		day = unlist(strsplit(day,"\\ "))
		day = substr(day[2],1,10)
	yr = as.numeric(substr(day,1,4))
	mon = as.numeric(substr(day,6,7))
	}
	else
	{
	day = as.character(dataread[1,1])
	day = unlist(strsplit(day,"\\ "))
	day = day[1]
	yr = as.numeric(substr(day,1,4))
	mon = as.numeric(substr(day,6,7))
	}
	}
	DA = format(round(nrow(dataread)/2.88,1),nsmall=1)
	eac = as.numeric(dataread[,3])
	tmstmpo = as.character(dataread[complete.cases(eac),1])
	{
		if(length(tmstmpo) > 1)
		{
			tmstmpo = tmstmpo[length(tmstmpo)]
		}
		else
		{
			{
				if(nrow(dataread) > 1)
				{
					tmstmpo = as.character(dataread[nrow(dataread),1])
				}
				else 
				{
					tmstmpo = "NA"
				}
			}
		}
	}
	eac = eac[complete.cases(eac)]
	{
	if(length(eac) > 1)
	{
	eaclast = format(round(eac[length(eac)],1),nsmall=1)
	stp1 = eac[length(eac)] - eac[1]
	eac = format(round(stp1,1),nsmall=1)
	}
	else
	{
	eac = "NA"
	eaclast = "NA"
	}
	}
	pac = as.numeric(dataread[,2])
	pac = pac[complete.cases(pac)]
	{
	if(length(pac)>1){
	pac = format(round(sum(pac)/12000,1),nsmall=1)}
	else
		pac="NA"
	}
	y1 = format(round(as.numeric(pac)/constYieldVal[mtno],2),nsmall=2)
	y2 = format(round(as.numeric(eac)/constYieldVal[mtno],2),nsmall = 2)
	IrrTot=TmodMean=PR1=PR2=NA
	if(ncol(dataread)>3)
	{
		d2 = dataread
		dataread = dataread[complete.cases(as.numeric(dataread[,4])),]
		IrrTot = round(sum(as.numeric(dataread[,4]))/6000,2)
		dataread = d2
		dataread = dataread[complete.cases(as.numeric(dataread[,4])),]
		TmodMean = round(mean(as.numeric(dataread[,5])),1)
		PR1 = round((as.numeric(y1)*100/IrrTot),1)
		PR2 = round((as.numeric(y2)*100/IrrTot),1)
		dataread = d2
	}
	df = data.frame(Date = day,DA = DA,EacM1=pac,EacM2=eac,Yield1=y1,Yield2=y2,EacLast=eaclast,TmLast=tmstmpo,IrrTot=IrrTot,TmodMean=TmodMean,PR1=PR1,PR2=PR2)
	write.table(df,file = pathw,row.names = F,col.names = T,sep = "\t",append = F)
}

thirdGenData = function(pathr,pathw)
{
	pathr = read.table(pathr,header = T,sep = "\t")
	if(!file.exists(pathw))
	{
		write.table(pathr,file = pathw,row.names = F,col.names = T,sep = "\t",append = F)
		return()
	}
	write.table(pathr,file = pathw,row.names = F,col.names = F,sep = "\t",append = T)
}
