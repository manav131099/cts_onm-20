import pandas as pd
import datetime
import pytz
import pyodbc
import os
import smtplib

#send mail function
def send_mail(date,data,rec):
    SERVER = "smtp.office365.com"
    FROM = 'operations@cleantechsolar.com'
    recipients = ['andre.nobre@cleantechsolar.com','rupesh.baker@cleantechsolar.com','rohit.jaswal@cleantechsolar.com','adam.syed@cleantechsolar.com','vicente.tangcueco@cleantechsolar.com','anusha.agarwal@cleantechsolar.com','sai.pranav@cleantechsolar.com']# must be a list
    TO=", ".join(recipients)
    SUBJECT = 'Met Station Digest '+date
    text2=str(data) 
    TEXT = text2
    # Prepare actual message
    message = "From:"+FROM+"\nTo:"+TO+"\nSubject:"+ SUBJECT +"\n\n"+TEXT
    # Send the mail
    server = smtplib.SMTP(SERVER)
    server.starttls()
    server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
    server.sendmail(FROM, recipients, message)
    server.quit()

dictionary = {}
stns=['KH-714','IN-711','IN-713','IN-721','MY-720','SG-724','VN-722']
path='/home/admin/Dropbox/Second Gen/'
tz = pytz.timezone('Asia/Calcutta')
date=(datetime.datetime.now(tz)+datetime.timedelta(days=-1)).strftime("%Y-%m-%d")
msg='\n'
#get ghi values from the stations
for j in stns:
    if(os.path.exists(path+'['+j+'S]/'+date[0:4]+'/'+date[0:7]+'/['+j+'S] '+date+'.txt')):
        df=pd.read_csv(path+'['+j+'S]/'+date[0:4]+'/'+date[0:7]+'/['+j+'S] '+date+'.txt',sep='\t')
        if(j=='KH-714'):
            dictionary[j]=df.loc[:,'Gsi'].values[0]
        else:
            dictionary[j]=df.loc[:,'Gsi01'].values[0]
    else:
        dictionary[j]=999
#sorting the stations according to their GHI values in ascending order
stns_order = []
for i in sorted(dictionary.items(),key=lambda x : x[1]):
    stns_order.append(i[0])
    
for j in stns_order:
    if(os.path.exists(path+'['+j+'S]/'+date[0:4]+'/'+date[0:7]+'/['+j+'S] '+date+'.txt')):
        df=pd.read_csv(path+'['+j+'S]/'+date[0:4]+'/'+date[0:7]+'/['+j+'S] '+date+'.txt',sep='\t')
        if(j=='KH-714'):
            ghi=df.loc[:,'Gsi'].values[0]
        else:
            ghi=df.loc[:,'Gsi01'].values[0]
        tamb=df.loc[:,'Tamb'].values[0]
        hamb=df.loc[:,'Hamb'].values[0]
        da=df.loc[:,'DA'].values[0]
        print(df)
        msg=msg+'-----------------------------------------\nO&M Code: '+j+'\n-----------------------------------------\n\n'
        msg=msg+'DA [%]: '+str(da)+'\n\n'
        msg=msg+'GHI [kWh/m^2]: '+str(ghi)+'\n\n'
        msg=msg+'Tamb [C]: '+str(tamb)+'\n\n'
        msg=msg+'Hamb [C]: '+str(hamb)
    else:
        msg=msg+'-----------------------------------------\nO&M Code: '+j+'\n-----------------------------------------\n\nData Not Available'
    msg=msg+'\n\n'
        
send_mail(date,msg,'rec')

