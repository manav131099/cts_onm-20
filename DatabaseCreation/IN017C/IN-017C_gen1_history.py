import urllib.request, json
import csv,time
import datetime
import collections
import os
import re
import pytz
from functools import reduce

#Global declarations
wordDictI = {
'i1':'Id','i2':'Inverter_Id','i3':'Fb_Id','i4':'DTT','i5':'Device_Time','i6':'SES','i7':'Salve_Id','i8':'Function_Code','i9':'AC_Voltage_Line1','i10':'AC_Voltage_Line2',
'i11':'AC_Voltage_Line3','i12':'AC_Current_Line1','i13':'AC_Current_Line2','i14':'AC_Current_Line3','i15':'AC_Power','i16':'AC_Power_Percentage','i17':'AC_Frequency',
'i18':'Power_Factor','i19':'Reactive_Power','i20':'DC_Current','i21':'DC_Power','i22':'Inverter_Temprature','i23':'Time_Of_Use_Today','i24':'Time_Of_Use_Life',
'i25':'Energy_Produced','i26':'KWH_Counter','i27':'MWH_Counter','i28':'GWH_Counter','i29':'DC_Voltage','i30':'Inverter_Status','i31':'Energy_Generated_Today',
'i32':'Tstamp','i33':'Total_Energy_Generated_Till','i34':'Inverter_Communication_Status','i35':'AC_Power_2','i36':'AC_Power_3','i37':'AC_Frequency_2',
'i38':'AC_Frequency_3','i39':'DC_Voltage_2','i40':'DC_Power_2','i41':'DC_Current_2','i42':'Plant_Id','i43':'Inv_Status_Word','i44':'Grid_Conn_Status',
'i45':'AC_Current_Totoal','i46':'AC_Volatge_RY','i47':'AC_Volatge_YB','i48':'AC_Volatge_BR','i49':'Apparent_Power','i50':'Event_Flag_1',
'i51':'Event_Flag_2','i52':'Event_Flag_3','i53':'Event_Flag_4','i54':'Coolent_Temp'
}

wordDictM = {
'm8':'Voltage_R_Phase','m9':'Voltage_Y_Phase','m10':'Voltage_B_Phase','m11':'Average_Voltage','m12':'Voltage_R_Y','m13':'Voltage_Y_B','m14':'Voltage_B_R',
'm15':'Line_To_Line_Voltage_Average','m16':'Current_R','m17':'Current_Y','m18':'Current_B','m19':'Current_N','m20':'Average_Current','m21':'Frequency_R',
'm22':'Frequency_Y','m23':'Frequency_B','m24':'Average_Frequency','m25':'Power_Factor_R','m26':'Power_Factor_Y','m27':'Power_Factor_B','m28':'Average_Power_Factor',
'm29':'Active_Power_R','m30':'Active_Power_Y','m31':'Active_Power_B','m32':'Total_Power','m33':'Reactive_Power_R','m34':'Reactive_Power_Y','m35':'Reactive_Power_B',
'm36':'Total_Reactive_Power','m37':'Total_Apparent_Power','m38':'Active_Energy','m39':'Reactive_Energy','m40':'Apparent_Power_R','m41':'Apparent_Power_Y',
'm42':'Apparent_Power_B','m43':'Reactive_Energy2','m44':'Maximum_Active_Power','m45':'Minimum_Active_Power','m46':'Maximum_Reactive_Power','m47':'Minimum_Reactive_Power',
'm48':'Maximum_Apparent_Power','m49':'Wh_Received','m50':'VAh_Received','m51':'VARh_Inductive_Received','m52':'VARh_Capacitive_Received','m53':'Energy_Export1',
'm54':'VAh_Delivered','m55':'VARh_Ind_Delivered','m56':'VARh_Cap_Delivered','m57':'THD','m58':'Active_Total_Import','m59':'Active_Total_Export',
'm60':'Apparent_Import','m61':'Aparent_Export','m62':'Energy_Today','m63':'Tstamp','m64':'Energy_Import','m65':'Energy_Export','m67':'Total_KW_Avg'
 }

wordDictW = {
'w9':'Humidity_Min','w10':'Module_Temp1','w11':'Wind_Direction','w12':'Wind_Speed','w13':'Ambient_Temp','w14':'Module_Temp2_Actual','w15':'Humidity_Max',
'w16':'Humidity_Actual','w17':'Ambient_Temp_Min','w18':'Ambient_Temp_Max','w19':'Ambient_Temp_Avg','w20':'Global_Irradiation_Min','w21':'Irradiation_Tilt1_Actual',
'w22':'Irradiation_Tilt2_Actual','w23':'Tstamp','w24':'Global_Irradiation_Max','w25':'Global_Irradiation_Avg','w26':'Wind_Speed_Min','w27':'Wind_Speed_Max',
'w28':'Humidity_Avg','w29':'Wind_Direction_Min','w30':'Wind_Direction_Max','w31':'Wind_Speed_Avg','w32':'Global_Irradiation_Actual','w34':'Rain','w35':'Room_Temperature'
    }



#Function to read start date
def read_date():
##    file = open('F:/Flexi_final/[IN-017C]/Gen-1/IN017CG.txt','r') ###local
    file = open('/home/admin/Start/IN017CG.txt','r') ###server
    date_read = file.read(10)
    return date_read


#Function to set start date
def set_date():
    date_read = read_date()
    global date
    date = datetime.datetime.strptime(date_read, '%Y-%m-%d').date()
    date += datetime.timedelta(days=-1) #To reduce date by a day as it gets added in current_date()

#Function to determine the next day
def determine_date():
    global date
    date += datetime.timedelta(days=1)
    return date

#Function to determine path and create directory 
def determine_path(curr_date,device,fd):
    year = curr_date.strftime("%Y")
    month = curr_date.strftime("%Y-%m")
    day = curr_date.strftime("%Y-%m-%d")
##    master_path = 'F:/del/FlexiMC_Data/Gen1_Data/[IN-017C]/' ###local
    master_path = '/home/admin/Dropbox/FlexiMC_Data/Gen1_Data/[IN-017C]/' ###server
    year_path = master_path+year+'/'
    month_path = year_path+month+'/'
    device_path = month_path+device+'/'
    final_path = device_path+'[IN-017C]-'+fd+'-'+day+'.txt'
    
    if not os.path.exists(year_path):
        os.makedirs(year_path)
    if not os.path.exists(month_path):
        os.makedirs(month_path)
    if not os.path.exists(device_path):
        os.makedirs(device_path)
    if os.path.exists(final_path):
        os.remove(final_path)
        print('[IN-017C]-'+fd+'-'+day+'.txt'+' removed')
    
    return final_path

#Function to replace header names 
def multiple_replace(text,name_head):

  if name_head == 'inverter':
      for key in wordDictI:
          text = re.sub(r"\b%s\b" % key, wordDictI[key],text)
      return text

  if name_head == 'MFM':
      for key in wordDictM:
          text = re.sub(r"\b%s\b" % key, wordDictM[key],text)
      return text

  if name_head == 'WMS':
      for key in wordDictW:
          text = re.sub(r"\b%s\b" % key, wordDictW[key],text)
      return text
    

#Function to reorder columns 
def reorder(line,name_head):
    if name_head == 'inverter':
        col = 31
    elif name_head == 'MFM':
        col = 62
    else:
        col = 22
    
    
    split = line.split('\t')
    temp = split[0]
    split[0] = split[col]
    split[col] = temp
    join = '\t'.join(split)
    return join


#Function to join average line
def get_avg(start_time,no_col,value,name_head):
    avg_line = str(start_time)
    for i in range(1,no_col):
        avg_line = avg_line + '\t'+str(value[i])
    if name_head == 'inverter':
        avg_line = avg_line + '\n'
    return avg_line


#Function to compute average values
def cmpt_avg(no_col,value,occ,counter):
    for i in range(no_col):
        try:
            value[i] = float(value[i])
            if value[i]!= 0:
                if i == 32:
                    value[i] = value[i]/(occ-counter)
                else:
                    value[i] = value[i]/occ
                    value[i] = round(value[i],7)
        
        except ValueError:
            pass
    return value
      


#Function to rename and reorder files 
def read_write_ops(name_head,read_path,write_path,curr_date):
    with open(read_path, 'r') as read_file, open(write_path, 'a') as write_file :
                print('reading '+ read_path)
                data = read_file.readlines()
                lines = []
                header = 0
                occ = 0
                counter = 0
                
                start_time = datetime.datetime.strptime(str(curr_date), "%Y-%m-%d")
                next_date = curr_date + datetime.timedelta(days=1)  # to get the last minute of day 
                end_time = datetime.datetime.strptime(str(next_date), "%Y-%m-%d")
                for line in data:
                    line = reorder(line,name_head)  
                    if header == 0:
                        line = multiple_replace(line,name_head)
                        lines.append(line)
                        header = 1
                    else:
                        split = line.split('\t')
                        no_col = len(split)
                        timestamp = split[0]
                        timestamp = timestamp[:16] #To remove seconds
                        curr_time = datetime.datetime.strptime(timestamp, "%Y-%m-%d %H:%M")

                        while start_time < curr_time:
                            if occ>0:
                                value = cmpt_avg(no_col,value,occ,counter)
                                if name_head == 'inverter':                                    
                                    if value[32] != 0:
                                        line = get_avg(start_time,no_col,value,name_head)
                                        lines.append(line)
                                    else:
                                        print('Skipping ',str(start_time),' as Total Energy is 0')
                                else:
                                    line = get_avg(start_time,no_col,value,name_head)
                                    lines.append(line)
                    
                            start_time+=datetime.timedelta(minutes=5)
                            occ = 0
                            counter = 0
                            

                        if occ == 0:
                            value = [0]*no_col
                        if(curr_time <= start_time):
                            occ +=1
                            for i in range(no_col):
                                try:
                                    if name_head == 'inverter' and i == 32:
                                        if split[i] == '0' or split[i] == 'NA' or split[i] == 'NULL':
                                            counter += 1
                                    value[i] += float(split[i])
                                except ValueError: ##to check if string
                                    value[i] = split[i]
                                except TypeError:
                                    if value[i] == 'NULL' or value[i] == 'NA': ## If NULL/NA is first, then can't do 'value[i] += float(split[i])' operation
                                        value[i] = 0
                                        value[i] += float(split[i])

                ## to add remaining rows (needed for last record as it won't enter 'while start_time < curr_time:' loop as no values present after that)       
                if occ>0 and start_time<end_time: 
                    value = cmpt_avg(no_col,value,occ,counter)
                    if name_head == 'inverter':                                    
                        if value[32] != 0:
                            line = get_avg(start_time,no_col,value,name_head)
                            lines.append(line)
                        else:
                            print('Skipping ',str(start_time),' as Total Energy is 0')
                    else:
                        line = get_avg(start_time,no_col,value,name_head)
                        lines.append(line)
                
                write_file.writelines(lines)
                print('Writing '+ write_path)
             

def meter(name_head,folder_name,file_field):
    tz = pytz.timezone('Asia/Kolkata')
    while True:
        today_date = datetime.datetime.now(tz).date()
        curr_date = determine_date()
        write_path = determine_path(curr_date,folder_name,file_field)
        read_path = write_path.replace('Dropbox/FlexiMC_Data/Gen1_Data', 'Data/Flexi_Raw_Data')
        
        try:
            if not os.path.exists(read_path):
                raise Exception('Empty')
            
                
        except Exception as e:
            if curr_date > today_date:
                print('Last file - Exiting while loop')
                break
            else:
                print('No file present - '+ str(e))
                continue

        read_write_ops(name_head,read_path,write_path,curr_date)      
        
            
set_date()
meter('inverter','Inverter_1','I1')
set_date()
meter('inverter','Inverter_2','I2')
set_date()
meter('inverter','Inverter_3','I3')

set_date()
meter('MFM','MFM','MFM')

set_date()
meter('WMS','WMS','WMS')


print('DONE!')





        

    
