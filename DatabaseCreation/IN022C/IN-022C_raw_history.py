import urllib.request, json
import csv,time
import datetime
import collections
import os
import pytz
import smtplib
import ssl

ssl._create_default_https_context = ssl._create_unverified_context
#global declarations
ihead = ['i1','i2','i3','i4','i5','i6','i7','i8','i9','i10','i11','i12','i13','i14','i15','i16','i17','i18','i19',
'i20','i21','i22','i23','i24','i25','i26','i27','i28','i29','i30','i31','i32','i33','i34','i35','i36','i37',
'i38','i39','i40','i41','i42','i43','i44','i45','i46','i47','i48','i49','i50','i51','i52','i53','i54']

mhead = ['m1','m2','m3','m4','m5','m6','m7','m8','m9','m10','m11','m12','m13','m14','m15','m16','m17','m18','m19','m20','m21','m22','m23',
'm24','m25','m26','m27','m28','m29','m30','m31','m32','m33','m34','m35','m36','m37','m38','m39','m40','m41','m42','m43','m44','m45',
'm46','m47','m48','m49','m50','m51','m52','m53','m54','m55','m56','m57','m58','m59','m60','m61','m62','m63','m64','m65','m66','m67']

whead = ['w1','w2','w3','w4','w5','w6','w7','w8','w9','w10','w11','w12','w13','w14','w15','w16','w17','w18','w19','w20','w21','w22','w23',
'w24','w25','w26','w27','w28','w29','w30','w31','w32','w33','w34','w35']



#Function to read start date
def read_date():
##    file = open('F:/Flexi_final/[IN-022C]/Raw/IN022C.txt','r') ###local
    file = open('/home/admin/Start/IN022C.txt','r') ###server
    date_read = file.read(10)
    return date_read

#Function to set start date
def set_date():
    date_read = read_date()
    global date
    date = datetime.datetime.strptime(date_read, '%Y-%m-%d').date()
    date += datetime.timedelta(days=-1) #To reduce date by a day as it gets added in current_date()

#Function to determine the next day
def determine_date():
    global date
    date += datetime.timedelta(days=1)
    return date


#Function to determine URL for the next day
def determine_url(date,url_head):
    replace_date = date.strftime("%Y%m%d")
    url = 'http://13.127.216.107/API/JSON/swl/BajajAutoPune/Data/'+replace_date+'/'+url_head+replace_date+'.json'
    return url

#Function to send mail
def sendmail():
    fromaddr = 'operations@cleantechsolar.com'
    toaddr = 'kn.rohan@gmail.com','andre.nobre@cleantechsolar.com','shravan1994@gmail.com','rupesh.baker@cleantechsolar.com'
    uname = 'shravan.karthik@cleantechsolar.com'

    subject = 'Bot IN-022C Raw Down - Portal Error'
    text = ' '
    message = 'Subject: {}\n\n{}'.format(subject,text)
    password = 'CTS&*(789'
    try:
        print('send try')
        server = smtplib.SMTP('smtp-mail.outlook.com',587)
        server.ehlo()
        server.starttls()
        server.login(uname,password)
        server.sendmail(fromaddr,toaddr,message)
        print('sending')
        server.quit()
    except Exception as e:
        print('mail send exception - ',str(e))



#Function to get header names
def get_header(json_head):
    if json_head == 'inverter':
        return ihead
    elif json_head == 'MFM':
        return mhead
    else:
        return whead


#Function to re-try connection
def connection_try(url):
    counter = 0
    while True:
        counter += 1
        try:
            print('second attempt try - ',counter)
            with urllib.request.urlopen(url) as url:
                try_data = json.loads(url.read().decode(), object_pairs_hook=collections.OrderedDict)
                return
        except Exception as e:
            if 'IncompleteRead' in str(e) or 'Errno 111' in str(e) or 'Remote end closed' in str(e) or 'urlopen error' in str(e):
                if counter == 4:
                    sendmail()
                    exit('Exiting due to persistent connection error')
                    
                print('sleeping for 15')
                time.sleep(900)
            else:
                return


#Function to determine path and create directory 
def determine_path(curr_date,device,fd):
    year = curr_date.strftime("%Y")
    month = curr_date.strftime("%Y-%m")
    day = curr_date.strftime("%Y-%m-%d")
##    master_path = 'F:/del/FlexiMC_Data/Raw_Data/[IN-022C]/' ###local
    master_path = '/home/admin/Data/Flexi_Raw_Data/[IN-022C]/' ###server
    year_path = master_path+year+'/'
    month_path = year_path+month+'/'
    device_path = month_path+device+'/'
    final_path = device_path+'[IN-022C]-'+fd+'-'+day+'.txt'
    
    if not os.path.exists(year_path):
        os.makedirs(year_path)
    if not os.path.exists(month_path):
        os.makedirs(month_path)
    if not os.path.exists(device_path):
        os.makedirs(device_path)
    if os.path.exists(final_path):
        os.remove(final_path)
        print('[IN-022C]-'+fd+'-'+day+'.txt'+' removed')
    
    return final_path


#Function to determine path and create directory 
def meter(url_head,json_head,folder_name,file_field):
    tz = pytz.timezone('Asia/Kolkata')
    while True:
        today_date = datetime.datetime.now(tz).date()
        curr_date = determine_date()
        url = determine_url(curr_date,url_head)
        path = determine_path(curr_date,folder_name,file_field)
        if curr_date > today_date:
            print('Last file - Exiting while loop',str(curr_date))
            break
        try:
            with urllib.request.urlopen(url) as url:
                load_data = json.loads(url.read().decode(), object_pairs_hook=collections.OrderedDict)
        except Exception as e:
            if 'IncompleteRead' in str(e) or 'Errno 111' in str(e) or 'Remote end closed' in str(e) or 'urlopen error' in str(e):
                print('Incomplete read/connection refuse error - 1  Slepping for 60 min - ',str(e))
                time.sleep(3600)
                connection_try(url)
                try:
                    print('Last attempt try')
                    with urllib.request.urlopen(url) as url:
                        load_data = json.loads(url.read().decode(), object_pairs_hook=collections.OrderedDict)
                except Exception as e:
                    if 'IncompleteRead' in str(e) or 'Errno 111' in str(e) or 'Remote end closed' in str(e) or 'urlopen error' in str(e):
                        print('Incomplete read/connection refuse error - 2  Killing bot and sending mail - ',str(e))
                        sendmail()
                        exit('Exiting due to persistent connection error')
                        
                    else:
                        if curr_date > today_date:
                            print('Last file -2 - Exiting while loop',str(curr_date), str(e))
                            break
                        else:
                            print('No data found -2 - ',str(curr_date), str(e))
                            continue                 
                    


            else:
                if curr_date > today_date:
                    print('Last file - Exiting while loop',str(curr_date), str(e))
                    break
                else:
                    print('No data found - ',str(curr_date), str(e))
                    continue
            
        new_file = 1
        i=0
        if load_data[json_head] == None:
            print('NULL Returned by Flexi')
            continue
        length = len(load_data[json_head])
        while i < length:
            data = load_data[json_head][i]
            with open(path, 'a',newline='') as csv_file:
                csvwriter = csv.writer(csv_file, delimiter='\t')
                line = []
                header = []
                
                header = get_header(json_head)
                line = [data.get(k,'NA') for k in header]
                line = ['NULL' if w is None else w for w in line]

                if new_file == 1:
                    csvwriter.writerow(header)
                    new_file = 0
                csvwriter.writerow(line)
                csv_file.close()
            i = i+1
        print(str(length)+' rows have been written to - '+path)



set_date()
meter('6_Inverter_5_','inverter','Inverter_1','I1')
set_date()
meter('6_Inverter_13_','inverter','Inverter_2','I2')
set_date()
meter('6_Inverter_12_','inverter','Inverter_3','I3')
set_date()
meter('6_Inverter_9_','inverter','Inverter_4','I4')
set_date()
meter('6_Inverter_1_','inverter','Inverter_5','I5')
set_date() 
meter('6_Inverter_8_','inverter','Inverter_6','I6')
set_date()
meter('6_Inverter_7_','inverter','Inverter_7','I7')
set_date()
meter('6_Inverter_6_','inverter','Inverter_8','I8')
set_date()
meter('6_Inverter_10_','inverter','Inverter_9','I9') 
set_date()
meter('6_Inverter_11_','inverter','Inverter_10','I10')
set_date()
meter('6_Inverter_14_','inverter','Inverter_11','I11')
set_date()
meter('6_Inverter_3_','inverter','Inverter_12','I12')
set_date()
meter('6_Inverter_2_','inverter','Inverter_13','I13')
set_date()
meter('6_Inverter_4_','inverter','Inverter_14','I14')
set_date()
meter('6_Inverter_15_','inverter','Inverter_15','I15')
set_date()
meter('6_Inverter_16_','inverter','Inverter_16','I16')
set_date()
meter('6_Inverter_17_','inverter','Ph2_Inverter_1','P2I1')
set_date()
meter('6_Inverter_18_','inverter','Ph2_Inverter_2','P2I2')
set_date()
meter('6_Inverter_19_','inverter','Ph2_Inverter_3','P2I3')
set_date()
meter('6_Inverter_21_','inverter','Ph2_Inverter_4','P2I4')
set_date()
meter('6_Inverter_20_','inverter','Ph2_Inverter_5','P2I5')
set_date()
meter('6_Inverter_23_','inverter','Ph2_Inverter_6','P2I6')
set_date()
meter('6_Inverter_22_','inverter','Ph2_Inverter_7','P2I7')
set_date()
meter('6_Inverter_25_','inverter','Ph2_Inverter_8','P2I8')
set_date()
meter('6_Inverter_24_','inverter','Ph2_Inverter_9','P2I9')



set_date()
meter('6_MFM_1_','MFM','OG1','OG1')
set_date()
meter('6_MFM_2_','MFM','OG2','OG2')
set_date()
meter('6_MFM_3_','MFM','OG3','OG3')
set_date()
meter('6_MFM_4_','MFM','OG4','OG4')

set_date()
meter('6_WMS_1_','WMS','WMS','WMS')

print('DONE!')




        

    
