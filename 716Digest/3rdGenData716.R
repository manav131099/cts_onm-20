
nodays = function(y,x)
{
  x = as.numeric(x)
  y = as.numeric(y)
 {
    
  if(x == 1 || x == 3 || x == 5 || x == 7 || x == 8 || x == 10 || x == 12)
  {
    return(31)
  }
  else if(x == 4 || x == 6 || x == 9 || x == 11)
  {
    return(30)
  }
  else if(x == 2 && y %% 4 == 0 )
  {
    return(29)
  }
  else
  {
    return(28)
  }
}
}
monthlyirr1 = 0
dayssofarmonth = 0
daystruemonth = 0
last30days = last30days2 = vector('numeric',30)
last30daysidx = 1
dropsoil = 0
dropsnp = 0
daysactive = 0
globirr1 = 0
dobstation = "06 Jan 2017"

path = "/home/admin/Dropbox/Second Gen/[SG-003S]"
years = dir(path)
for(x in 1 : length(years))
{
 pathmonth = paste(path,years[x],sep="/")
 months = dir(pathmonth)
 for(y in 1 : length(months))
 {
   monthlyirr1  = 0
   pathdays = paste(pathmonth,months[y],sep="/")
   strng = unlist(strsplit(months[y],"-"))
   daystruemonth = nodays(strng[1],strng[2])
   dayssofarmonth = 0
   days = dir(pathdays)
   sumfilename = paste("[SG-003S] ",substr(months[y],3,4),substr(months[y],6,7),".txt",sep="")
   for(z in 1 : length(days))
   {
     if(days[z] == sumfilename)
     {
       next
     }
     dataread = read.table(paste(pathdays,days[z],sep="/"),header = T,sep="\t")
     monthlyirr1 = monthlyirr1 + as.numeric(dataread[1,3])
     globirr1 = globirr1 + as.numeric(dataread[1,3])
     dayssofarmonth = dayssofarmonth + 1    
     daysactive = daysactive + 1
		 print(paste(days[z],"is",daysactive))
     last30daysidx = (last30daysidx +1 )%% 31
     if(last30daysidx == 0) {last30daysidx =1}
   }
 }
}

LAST30DAYSTOT = sum(last30days)
LAST30DAYSMEAN = mean(last30days)
LAST30DAYSTOT2 = sum(last30days2)
LAST30DAYSMEAN2 = mean(last30days2)
DAYSACTIVE = daysactive
MONTHLYIRR1 = monthlyirr1
MONTHAVG1 = monthlyirr1 / dayssofarmonth
FORECASTIRR1 = MONTHAVG1 * daystruemonth

LAST30DAYSTOT=rf(LAST30DAYSTOT) 
LAST30DAYSMEAN=rf(LAST30DAYSMEAN) 
LAST30DAYSTOT2=rf(LAST30DAYSTOT2) 
LAST30DAYSMEAN2=rf(LAST30DAYSMEAN2)
DAYSACTIVE=rf(DAYSACTIVE) 
MONTHLYIRR1=rf(MONTHLYIRR1)
MONTHAVG1=rf(MONTHAVG1)
FORECASTIRR1=rf(FORECASTIRR1) 
