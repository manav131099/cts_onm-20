errHandle = file('/home/admin/Logs/Logs727Mail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
system('rm -R "/home/admin/Dropbox/Second Gen/[SG-727S]"')
source('/home/admin/CODE/SGDigest/SG727Digest/runHistory727.R')
require('mailR')
print('History done')
	source('/home/admin/CODE/SGDigest/SG724Digest/3rdGenData724.R')
print('3rd gen data called')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
source('/home/admin/CODE/SGDigest/SG727Digest/aggregateInfo.R')
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
  min = as.numeric(hr[2])
  hr = as.numeric(hr[1])
  return((hr * 60 + min + 1))
}
rf = function(x)
{
  return(format(round(x,2),nsmall=2))
}
rf1 = function(x)
{
  return(format(round(x,1),nsmall=1))
}
rf3 = function(x)
{
  return(format(round(x,3),nsmall=3))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

preparebody = function(path)
{
  print("Enter Body Func")
  body = ""
  body = paste(body,"Site Name: GlobalFoundries Woodlands (Fab 7G)\n",sep="")
  body = paste(body,"\nLocation: 60 Woodlands Industrial Park Street 2, Singapore 738406\n")
  body = paste(body,"\nO&M Code: SG-005\n")
  body = paste(body,"\nSystem Size: 486 kWp\n")
  body = paste(body,"\nNumber of Energy Meters: 1\n")
  body = paste(body,"\nModule Brand / Model / Nos: Trina Solar/Trina Solar TSM DE15M(II) 405/ 1,200\n")
  body = paste(body,"\nInverter Brand / Model / Nos: SMA / Solid-Q 50 / 7\n")
  body = paste(body,"\nSite COD: 2019-12-31\n")
  body = paste(body,"\nSystem sections:\n")
  body = paste(body,"\nFab 7G: 486 kWp\n")
  #body = paste(body,"\nSystem age [days]:",(DAYSACTIVE),"\n")
  #body = paste(body,"\nSystem age [days]:",rf(DAYSACTIVE/365))
  
  for(x in 1 : length(path))
  {
    print(path[x])
    body = paste(body,"\n_____________________________________________\n")
    days = unlist(strsplit(path[x],"/"))
    days = substr(days[length(days)],11,20)
    body = paste(body,days)
    body  = paste(body,"\n_____________________________________________\n")
    dataread = read.table(path[x],header = T,sep="\t",stringsAsFactors=F)
    print("===================")
    print(path[x])
    print(ncol(dataread))
    body = paste(body,"\nPts Recorded: ",as.character(dataread[1,2])," (",round(as.numeric(dataread[1,3])),"%)",sep="")
    body = paste(body,"\n\nEac-1 [kWh]: ",as.character(dataread[1,4]),sep="")
    body = paste(body,"\n\nEac-2 [kWh]: ",as.character(dataread[1,5]),sep="")
    body = paste(body,"\n\nYield-1 [kWh/kWp]: ",as.character(dataread[1,6]),sep="")
    body = paste(body,"\n\nYield-2 [kWh/kWp]: ",as.character(dataread[1,7]),sep="")
    body = paste(body,"\n\nIrradiation (source SG-724S) [kWh/m2]: ",as.character(dataread[1,8]),sep="")
    body = paste(body,"\n\nPR-1 [%]: ",as.character(dataread[1,9]),sep="")
    body = paste(body,"\n\nPR-2 [%]: ",as.character(dataread[1,10]),sep="")
    body = paste(body,"\n\nLast recorded time: ",as.character(dataread[1,11]),sep="")
    body = paste(body,"\n\nLast recorded reading (delivered) [kWh]: ",as.character(dataread[1,12]),sep="")
    body = paste(body,"\n\nLast recorded reading (received) [kWh]: ",as.character(dataread[1,13]),sep="")
    body = paste(body,"\n\nLast recorded reading (net) [kWh]: ",as.character(dataread[1,14]),sep="")
    body = paste(body,"\n")
    print("==============")
  }
  print('Daily Summary for Body Done')
  return(body)
}

path = "/home/admin/Dropbox/Cleantechsolar/1min/[727]"
pathwrite = "/home/admin/Dropbox/Second Gen/[SG-727S]"
checkdir(pathwrite)
date = format(Sys.time(),tz = "Singapore")
years = dir(path)
prevpathyear = paste(path,years[length(years)],sep="/")
months = dir(prevpathyear)
prevsumname = paste("[SG-727S] ",substr(months[length(months)],3,4),substr(months[length(months)],6,7),".txt",sep="")
prevpathmonth = paste(prevpathyear,months[length(months)],sep="/")
currday = prevday = dir(prevpathmonth)[length(dir(prevpathmonth))]
prevwriteyears = paste(pathwrite,years[length(years)],sep="/")
prevwritemonths = paste(prevwriteyears,months[length(months)],sep="/")
#if(length(dir(pathmonth)) > 1)
#{
#  prevday = dir(pathmonth[length(dir(pathmonth))-1])
#}

stnnickName = "727"
stnnickName2 = "SG-727S"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
if(!is.null(lastdatemail))
{
  yr = substr(lastdatemail,1,4)
  monyr = substr(lastdatemail,1,7)
  prevpathyear = paste(path,yr,sep="/")
  prevsumname = paste("[",stnnickName2,"] ",substr(lastdatemail,3,4),substr(lastdatemail,6,7),".txt",sep="")
  prevpathmonth = paste(prevpathyear,monyr,sep="/")
  currday = prevday = paste("[",stnnickName,"] ",lastdatemail,".txt",sep="")
  prevwriteyears = paste(pathwrite,yr,sep="/")
  prevwritemonths = paste(prevwriteyears,monyr,sep="/")
}

check = 0
sender <- "operations@cleantechsolar.com" 
uname <- "shravan.karthik@cleantechsolar.com"
pwd = 'CTS&*(789'
# recipients = getRecipients("SG-727S","m")
recipients = c('operationsSG@cleantechsolar.com','om-it-digest@cleantechsolar.com','om-interns@cleantechsolar.com','solarsg@cutechgroup.com')
wt =1
idxdiff = 1
while(1)
{
  # recipients = getRecipients("SG-727S","m")
  recipients = c('operationsSG@cleantechsolar.com','om-it-digest@cleantechsolar.com','om-interns@cleantechsolar.com','solarsg@cutechgroup.com')
  recordTimeMaster("SG-727S","Bot")
  years = dir(path)
  writeyears = paste(pathwrite,years[length(years)],sep="/")
  checkdir(writeyears)
  pathyear = paste(path,years[length(years)],sep="/")
  months = dir(pathyear)
  writemonths = paste(writeyears,months[length(months)],sep="/")
  sumname = paste("[SG-727S] ",substr(months[length(months)],3,4),substr(months[length(months)],6,7),".txt",sep="")
  checkdir(writemonths)
  pathmonth = paste(pathyear,months[length(months)],sep="/")
  if(length(dir(pathmonth)) < 1)
  {
    print('No file in the folder yet..')
    Sys.sleep(3600)
    next
  }
  tm = timetomins(substr(format(Sys.time(),tz = "Singapore"),12,16))
  currday = dir(pathmonth)[length(dir(pathmonth))]
  if(prevday == currday)
  {
    if(wt == 1)
    {
      print("")
      print("")
      print("__________________________________________________________")
      print(substr(currday,7,16))
      print("__________________________________________________________")
      print("")
      print("")
      print('Waiting')
      wt = 0
    }
    {
      if(tm > 1319 || tm < 30)
      {
        Sys.sleep(120)
      }        
      else
      {
        Sys.sleep(3600)
      }
    }
    next
  }
  print('New Data found.. sleeping to ensure sync complete')
  Sys.sleep(20)
  print('Sleep Done')
  idxdiff = match(currday,dir(pathmonth)) - match(prevday,dir(prevpathmonth))
  if(idxdiff < 0)
  {
    idxdiff = length(dir(prevpathmonth)) - match(prevday,dir(prevpathmonth)) + length(dir(pathmonth))
    newmonth = pathmonth
    newwritemonths = writemonths
    pathmonth = prevpathmonth
    writemonths = prevwritemonths
  }
  while(idxdiff)
  {
    {
      if(prevday == dir(prevpathmonth)[length(dir(prevpathmonth))])
      {
        pathmonth = newmonth
        writemonths = newwritemonths
        currday = dir(pathmonth)[1]
        monthlyirr1 = monthlyirr2 = monthlyirr3 = 0
        strng = unlist(strsplit(months[length(months)],"-"))
        dayssofarmonth = 0
      }	
      else
        currday = dir(pathmonth)[(match(prevday,dir(pathmonth)) +1)]
    }
    dataread = read.table(paste(pathmonth,currday,sep='/'),header = T,sep = "\t")
    datawrite = prepareSumm(dataread,1)
    datasum = rewriteSumm(datawrite)
    currdayw = gsub("727","SG-727S",currday)
    write.table(datawrite,file = paste(writemonths,currdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
    
    {
      if(!file.exists(paste(writemonths,sumname,sep="/")))
      {
        write.table(datasum,file = paste(writemonths,sumname,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
        print('Summary file created')
      }
      else 
      {
        write.table(datasum,file = paste(writemonths,sumname,sep="/"),append = T,sep="\t",row.names = F,col.names = F)
        print('Summary file updated')  
      }
    }
    
    filetosendpath = c(paste(writemonths,currdayw,sep="/"))
    filename = c(currday)
    filedesc = c("Daily Digest")
    dataread = read.table(paste(prevpathmonth,prevday,sep='/'),header = T,sep = "\t")
    datawrite = prepareSumm(dataread,1)
    prevdayw = gsub("727","SG-727S",prevday)
    
    dataprev = read.table(paste(prevwritemonths,prevdayw,sep="/"),header = T,sep = "\t")
    print('Summary read')
    if(as.numeric(datawrite[,2]) != as.numeric(dataprev[,2]))
    {
      print('Difference in old file noted')
      
      
      datasum = rewriteSumm(datawrite)
      prevdayw = gsub("727","SG-727S",prevday)
      
      write.table(datawrite,file = paste(prevwritemonths,prevdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
      
      
      datarw = read.table(paste(prevwritemonths,prevsumname,sep="/"),header = T,sep = "\t")
      rn = match(as.character(datawrite[,1]),as.character(datarw[,1]))
      print(paste('Match found at row no',rn))
      datarw[rn,] = datasum[1,]
      write.table(datarw,file = paste(prevwritemonths,prevsumname,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
      filetosendpath[2] = paste(prevwritemonths,prevdayw,sep="/")
      filename[2] = prevday
      filedesc[2] = c("Updated Digest")
      print('Updated old files.. both digest and summary file')
    }
    
    print('Sending mail')
    body = ""
    body = preparebody(filetosendpath)
    body = gsub("\n ","\n",body)
    
    send.mail(from = sender,
              to = recipients,
              subject = paste("Station [SG-727S] Digest",substr(currday,7,16)),
              body = body,
              smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
              authenticate = TRUE,
              send = TRUE,
              attach.files = filetosendpath,
              file.names = filename, # optional parameter
              file.descriptions = filedesc, # optional parameter
              debug = F)
    print('Mail Sent')
    recordTimeMaster("SG-727S","Mail",substr(currday,7,16))
    prevday = currday
    prevpathyear = pathyear
    prevpathmonth = pathmonth
    prevwriteyears = writeyears
    prevwritemonths = writemonths
    prevsumname = sumname
    idxdiff = idxdiff - 1;
    wt = 1
  }
  gc()
}
sink()

