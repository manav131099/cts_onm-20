system('rm -R "/home/admin/Dropbox/Second Gen/[SG-003E]/Preliminary-Settlements"')
system('rm -R "/home/admin/Dropbox/Third Gen/[SG-003E]/Preliminary-Settlements"')
system('rm -R "/home/admin/Dropbox/Fourth_Gen/[SG-003E]/Preliminary-Settlements"')

require('mailR')
require('Hmisc')

source('/home/admin/CODE/SGDigest/SG003EDigest/Prelim/SG003EPMailDigestFunctions.R')
source('/home/admin/CODE/Misc/calcBackLog.R')
path = '/home/admin/Dropbox/Gen 1 Data/[SG-003E]/Preliminary-Settlements'
writepath2G = '/home/admin/Dropbox/Second Gen/[SG-003E]/Preliminary-Settlements'
checkdir(writepath2G)

writepath3G = '/home/admin/Dropbox/Third Gen/[SG-003E]/Preliminary-Settlements'
checkdir(writepath3G)
writepath4G = "/home/admin/Dropbox/Fourth_Gen/[SG-003E]"
checkdir(writepath4G)
writepath4G = "/home/admin/Dropbox/Fourth_Gen/[SG-003E]/Preliminary-Settlements"
checkdir(writepath4G)

DAYSALIVE = 0
years = dir(path)
stnnickName2 = "SG-003EPrelim"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
stopDate = paste("[SG-003E-P] ",lastdatemail,".txt",sep="")
ENDCALL=0

for(x in 1 : length(years))
{
  pathyrs = paste(path,years[x],sep="/")
  writepath2Gyr = paste(writepath2G,years[x],sep="/")
  checkdir(writepath2Gyr)
  writepath3Gyr = paste(writepath3G,years[x],sep="/")
  checkdir(writepath3Gyr)
  months = dir(pathyrs)
  for(y in 1 : length(months))
  {
    MONTHTOTEAC1 = 0
    MONTHTOTEAC2 = 0
    DAYSTHISMONTH = 0
    pathdays= paste(pathyrs,months[y],sep="/")
    writepath2Gdays = paste(writepath2Gyr,months[y],sep="/")
    checkdir(writepath2Gdays)
    writepath3Gfinal = paste(writepath3Gyr,"/[SG-003E-P] ",months[y],".txt",sep="")
    days = dir(pathdays)
    temp = unlist(strsplit(days[1]," "))
    temp = as.Date(substr(temp[2],1,10),"%Y-%m-%d")
      for(t in 1 : length(days))
      {
     	 if(ENDCALL==1)
			 		 break
				{
          if(x == 1 && y == 1 && t == 1)
          {
            DOB = unlist(strsplit(days[t]," "))
            DOB = substr(DOB[2],1,10)
            DOB = as.Date(DOB,"%Y-%m-%d")
          }
         # else if(x == length(years) && y == length(months) && t == length(days))
         # {
         #   next
         # }
        }
        DAYSALIVE = DAYSALIVE + 1
        writepath2Gfinal = paste(writepath2Gdays,"/",days[t],sep="")
        readpath = paste(pathdays,days[t],sep="/")
        secondGenData(readpath,writepath2Gfinal)
        thirdGenData(writepath2Gfinal,writepath3Gfinal)
				if(stopDate == days[t])
				{
					ENDCALL = 1
				}
      }
			if(ENDCALL == 1)
				break
    }
			if(ENDCALL == 1)
				break
}
print('Historical Analysis Done')
