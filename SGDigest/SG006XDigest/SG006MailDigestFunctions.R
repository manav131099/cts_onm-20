rm(list = ls())
source('/home/admin/CODE/common/math.R')
TIMESTAMPSALARM = NULL
TIMESTAMPSALARM2 = NULL
TIMESTAMPSALARM3 = NULL
TIMESTAMPSALARM4 = NULL
METERCALLED = 0
ltcutoff = .001
CABLOSSTOPRINT = 0
instcaps = c(163.80,100.8,176.4,50.4,504)
FORCEWAIT = 0
getGTIData = function(date)
{
	yr=unlist(strsplit(date,"-"))
	yrmon =paste(yr[1],yr[2],sep="-")
	yr = yr[1]
	path = paste("/home/admin/Dropbox/Second Gen/[SG-001X]/",yr,"/",
	yrmon,"/[SG-001X] ",date,".txt",sep="")
	path2 = paste("/home/admin/Dropbox/Second Gen/[SG-003S]/",yr,"/",
	yrmon,"/[SG-003S] ",date,".txt",sep="")
	GTI=GTI2=NA
	Tot = NA
	if((!file.exists(path)) && FORCEWAIT)
		Sys.sleep(3600)
	if(file.exists(path))
	{
		dataread = read.table(path,header=T,sep="\t",stringsAsFactors=F,fill=TRUE)
		GTI = as.numeric(dataread[1,9])
	}
	if(file.exists(path2))
	{
		dataread = read.table(path2,header=T,sep="\t",stringsAsFactors=F,fill=TRUE)
		GTI2 = as.numeric(dataread[1,3])
	}
	return(c(GTI,GTI2))
}
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
	seq1 = seq(from = 1,to = length(hr),by =2)
	seq2 = seq(from = 2,to = length(hr),by =2)
  min = as.numeric(hr[seq2]) 
  hr = as.numeric(hr[seq1]) * 60
  return((hr + min + 1))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
secondGenData = function(filepath,writefilepath)
{ 
  {
		if(METERCALLED == 1){
	 		TIMESTAMPSALARM <<- NULL}
		else if(METERCALLED == 2){
			TIMESTAMPSALARM2 <<- NULL}
		else if(METERCALLED == 3){
			TIMESTAMPSALARM3 <<- NULL}
		else if(METERCALLED == 4){
			TIMESTAMPSALARM4 <<- NULL}
		else if(METERCALLED == 5){
			TIMESTAMPSALARM5 <<- NULL}
	}
	print(paste('IN 2G',filepath))
  dataread = read.table(filepath,header = T,sep = "\t",stringsAsFactors=F,fill=TRUE)
	dataread2 = dataread

	lasttime=lastread=Eac2=NA
	
		coleac1 = which(colnames(dataread2)=="Active.power..W.")
		coleac2 = which(colnames(dataread2)=="Active.energy.delivered..Wh.")
		if(METERCALLED==5){
			coleac1 = coleac1 + 1
			coleac2 = coleac2 + 1
		}
		dataread = dataread2[complete.cases(as.numeric(dataread2[,coleac2])),]
	if(nrow(dataread)>0)
	{
	lasttime = as.character(dataread[nrow(dataread),1])
	lastread = as.character(round(as.numeric(dataread[nrow(dataread),coleac2])/1000,3))
  Eac2 = format(round((as.numeric(dataread[nrow(dataread),coleac2]) - as.numeric(dataread[1,coleac2]))/1000,2),nsmall=1)
	}
	dataread = dataread2[complete.cases(as.numeric(dataread2[,coleac1])),]
	Eac1 = RATIO=NA
	if(nrow(dataread) > 1)
	{
   	Eac1 = format(abs(round(sum(as.numeric(dataread[,coleac1]))/60000,1)),nsmall=2)
		RATIO = round(as.numeric(Eac1)*100/as.numeric(Eac2),1)
	}
	dataread = dataread2
  DA = format(round(nrow(dataread)/14.4,1),nsmall=1)
  tdx = timetomins(substr(dataread[,1],12,16))
  dataread2 = dataread[tdx > 540,]
  tdx = tdx[tdx > 540]
  dataread2 = dataread2[tdx < 1020,]
	dataread2 = dataread2[complete.cases(as.numeric(dataread2[,coleac1])),]
  missingfactor = 480 - nrow(dataread2)
	{
  		dataread2 = dataread2[abs(as.numeric(dataread2[,coleac1])) < ltcutoff,]
	}
	if(length(dataread2[,1]) > 0)
	{
		{
			if(METERCALLED == 1){
				TIMESTAMPSALARM <<- as.character(dataread2[,1])}
			else if(METERCALLED == 2){
		 	 TIMESTAMPSALARM2 <<- as.character(dataread2[,1])}
			else if(METERCALLED == 3){
		 	 TIMESTAMPSALARM3 <<- as.character(dataread2[,1])}
			else if(METERCALLED == 4){
		 	 TIMESTAMPSALARM4 <<- as.character(dataread2[,1])}
			else if(METERCALLED == 5){
		 	 TIMESTAMPSALARM5 <<- as.character(dataread2[,1])}
		}
	}
  totrowsmissing = format(round((missingfactor + nrow(dataread2))/4.8,1),nsmall=1)
	date = NA
	if(nrow(dataread)>0)
		date = substr(as.character(dataread[1,1]),1,10)
	print(paste('Date',date))
	print(paste('EAC1',Eac1))
	print(paste('Eac2',Eac2))
	print(paste('RATIO',RATIO))
	print(paste('DA',DA))
	print(paste('Downtime',totrowsmissing))
	print(paste('Lasttime',lasttime))
	print(paste('LastRead',lastread))
	DSPY1 = round((as.numeric(Eac1)/instcaps[METERCALLED]),2)
	DSPY2 = round((as.numeric(Eac2)/instcaps[METERCALLED]),2)
	GTI = getGTIData(date)
	GTI2 = as.numeric(GTI[2])
	GTI=as.numeric(GTI[1])
	PR1 = round(DSPY1*100/GTI,1)
	PR2 = round(DSPY2*100/GTI,1)
	PR1SG003 = round(DSPY1*100/GTI2,1)
	PR2SG003 = round(DSPY2*100/GTI2,1)
  df = data.frame(Date = date, SolarPowerMeth1 = as.numeric(Eac1),SolarPowerMeth2 = as.numeric(Eac2),
              Ratio=RATIO,DA = DA,Downtime = totrowsmissing,
							LastTime = lasttime, LastRead = lastread,Yield1 = DSPY1, Yield2 = DSPY2,GSI=GTI, PR1=PR1, PR2=PR2,
							GTISG003=GTI2,PR1SG003=PR1SG003,PR2SG003=PR2SG003,
							stringsAsFactors = F)
	write.table(df,file = writefilepath,row.names = F,col.names = T,sep="\t",append = F)
  return(df)
}

thirdGenData = function(filepathm1, filepathm2,filepathm3,filepathm4,filepathm5,writefilepath)
{
  dataread1 =read.table(filepathm1,header = T,sep="\t",stringsAsFactors=F,fill=TRUE) #its correct dont change
  dataread2 = read.table(filepathm2,header = T,sep = "\t",stringsAsFactors=F,fill=TRUE) #its correct dont change
  dataread3 = read.table(filepathm3,header = T,sep = "\t",stringsAsFactors=F,fill=TRUE) #its correct dont change
  dataread4 = read.table(filepathm4,header = T,sep = "\t",stringsAsFactors=F,fill=TRUE) #its correct dont change
  {
	if(is.na(filepathm5) || (!file.exists(filepathm5)))
	{
		dataread5 = data.frame(Date = NA, SolarPowerMeth1 = NA,SolarPowerMeth2 = NA,
              Ratio=NA,DA = NA,Downtime = NA,
							LastTime = NA, LastRead = NA,Yield1 = NA, Yield2 = NA,GSI=NA, PR1=NA, PR2=NA,
							GTISG003=NA,PR1SG003=NA,PR2SG003=NA,
							stringsAsFactors = F)
	}
	else	
		dataread5 = read.table(filepathm5,header = T,sep = "\t",stringsAsFactors=F,fill=TRUE) #its correct dont change
	}
	dt = as.character(dataread1[,6])
	da = as.character(dataread1[,5])
	gti = as.character(dataread1[,11])
	gti2 = as.character(dataread1[,14])

	Eac11 = as.numeric(dataread1[,2])
	Eac12 = as.numeric(dataread1[,3])
	Eac21 = as.numeric(dataread2[,2])
	Eac22 = as.numeric(dataread2[,3])
	Eac31 = as.numeric(dataread3[,2])
	Eac32 = as.numeric(dataread3[,3])
	Eac41 = as.numeric(dataread4[,2])
	Eac42 = as.numeric(dataread4[,3])
	Eac51 = as.numeric(dataread5[,2])
	Eac52 = as.numeric(dataread5[,3])

	lastt1 = as.character(dataread1[,7])
	lastt2 = as.character(dataread2[,7])
	lastt3 = as.character(dataread3[,7])
	lastt4 = as.character(dataread4[,7])
	lastt5 = as.character(dataread5[,7])
	
	lastr1 = as.numeric(dataread1[,8])
	lastr2 = as.numeric(dataread2[,8])
	lastr3 = as.numeric(dataread3[,8])
	lastr4 = as.numeric(dataread4[,8])
	lastr5 = as.numeric(dataread5[,8])

	pr11 = as.numeric(dataread1[,12])
	pr21 = as.numeric(dataread2[,12])
	pr31 = as.numeric(dataread3[,12])
	pr41 = as.numeric(dataread4[,12])
	pr51 = as.numeric(dataread5[,12])
	
	pr12 = as.numeric(dataread1[,13])
	pr22 = as.numeric(dataread2[,13])
	pr32 = as.numeric(dataread3[,13])
	pr42 = as.numeric(dataread4[,13])
	pr52 = as.numeric(dataread5[,13])
	
	pr11sg003 = as.numeric(dataread1[,15])
	pr21sg003 = as.numeric(dataread2[,15])
	pr31sg003 = as.numeric(dataread3[,15])
	pr41sg003 = as.numeric(dataread4[,15])
	pr51sg003 = as.numeric(dataread5[,15])
	
	pr12sg003 = as.numeric(dataread1[,16])
	pr22sg003 = as.numeric(dataread2[,16])
	pr32sg003 = as.numeric(dataread3[,16])
	pr42sg003 = as.numeric(dataread4[,16])
	pr52sg003 = as.numeric(dataread5[,16])
	
	y11 = as.numeric(dataread1[,9])
	y21 = as.numeric(dataread2[,9])
	y31 = as.numeric(dataread3[,9])
	y41 = as.numeric(dataread4[,9])
	y51 = as.numeric(dataread5[,9])
	
	y12 = as.numeric(dataread1[,10])
	y22 = as.numeric(dataread2[,10])
	y32 = as.numeric(dataread3[,10])
	y42 = as.numeric(dataread4[,10])
	y52 = as.numeric(dataread5[,10])
	
	FullSite = Eac12 + Eac22 + Eac32 + Eac42
	idxuse = c(1,2,3,4)
	if(is.finite(Eac52))
	{
		FullSite = FullSite + Eac52
		idxuse = c(idxuse,5)
	}
	FullSiteYld = round(FullSite/sum(instcaps[idxuse]),2)
	FullSitePr = round(FullSiteYld*100/as.numeric(gti),1)
	FullSitePr2 = round(FullSiteYld*100/as.numeric(gti2),1)
	SdYld = round(sdp(c(y12,y22,y32,y42)),3)
	if(is.finite(y52))
		SdYld = round(sdp(c(y12,y22,y32,y42,y52)),3)
	CovYld = as.numeric(format(round(SdYld*100/mean(c(y12,y22,y32,y42)),1),nsmall=1))
	if(is.finite(y52))
	CovYld = as.numeric(format(round(SdYld*100/mean(c(y12,y22,y32,y42,y52)),1),nsmall=1))

	df = data.frame(Date = substr(as.character(dataread1[1,1]),1,10),
	DA= da,
	Downtime = dt,
	GTI=gti,
	Eac11 = Eac11,
	Eac12 = Eac12,
	Eac21 = Eac21,
	Eac22 = Eac22,
	Eac31 = Eac31,
	Eac32 = Eac32,
	Eac41 = Eac41,
	Eac42 = Eac42,
	LastT1=lastt1,
	LastT2=lastt2,
	LastT3=lastt3,
	LastT4=lastt4,
	LastR1=lastr1,
	LastR2=lastr2,
	LastR3=lastr3,
	LastR4=lastr4,
	PR11 = pr11,
	PR12 = pr12,
	PR21 = pr21,
	PR22 = pr22,
	PR31 = pr31,
	PR32 = pr32,
	PR41 = pr41,
	PR42 = pr42,
	Yld11 = y11,
	Yld12 = y12,
	Yld21 = y21,
	Yld22 = y22,
	Yld31 = y31,
	Yld32 = y32,
	Yld41 = y41,
	Yld42 = y42,
	FullSiteEac = FullSite,
	FullSiteYld = FullSiteYld,
	FullSitePr = FullSitePr,
	SdYld=SdYld,
	CovYld = CovYld,
	GTISG003=gti2,
	PR11SG003 = pr11sg003,
	PR12SG003 = pr12sg003,
	PR21SG003 = pr21sg003,
	PR22SG003 = pr22sg003,
	PR31SG003 = pr31sg003,
	PR32SG003 = pr32sg003,
	PR41SG003 = pr41sg003,
	PR42SG003 = pr42sg003,
	FullSitePRSG003=FullSitePr2,
	Eac51=Eac51,
	Eac52 = Eac52,
	LastT5 = lastt5,
	LastR5 = lastr5,
	PR51 = pr51,
	PR52 = pr52,
	Yld51 = y51,
	Yld52 = y52,
	PR51SG003=pr51sg003,
	stringsAsFactors=F)
  {
    if(file.exists(writefilepath))
    {
			data = read.table(writefilepath,header=T,sep="\t",stringsAsFactors=F,fill=TRUE)
			dates = as.character(data[,1])
			idxmtch = match(substr(as.character(dataread1[1,1]),1,10),dates)
			if(is.finite(idxmtch))
			{
				data = data[-idxmtch,]
				write.table(data,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
			}
      write.table(df,file = writefilepath,row.names = F,col.names = F,sep = "\t",append = T)
    }
    else
    {
      write.table(df,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
    }
  }
}