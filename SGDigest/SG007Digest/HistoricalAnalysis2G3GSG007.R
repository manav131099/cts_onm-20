system('rm -R "/home/admin/Dropbox/Second Gen/[SG-007X]"')
system('rm -R "/home/admin/Dropbox/Third Gen/[SG-007X]"')
system('rm -R "/home/admin/Dropbox/Fourth_Gen/[SG-007X]"')

require('mailR')
require('Hmisc')

source('/home/admin/CODE/SGDigest/SG007Digest/SG007MailDigestFunctions.R')
source('/home/admin/CODE/Misc/calcBackLog.R')
path = '/home/admin/Dropbox/Gen 1 Data/[SG-007X]'
writepath2G = '/home/admin/Dropbox/Second Gen/[SG-007X]'
irrpath = '/home/admin/Dropbox/Cleantechsolar/1min/[714]'
checkdir(writepath2G)

writepath3G = '/home/admin/Dropbox/Third Gen/[SG-007X]'
checkdir(writepath3G)
writepath4G = "/home/admin/Dropbox/Fourth_Gen/[SG-007X]"
checkdir(writepath4G)
timeStampAzureFile = "/home/admin/Start/SG007XA4G.txt"

DAYSALIVE = 0
years = dir(path)
stnnickName2 = "SG-007X"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
stopDate = paste("[",stnnickName2,"] ",lastdatemail,".txt",sep="")
ENDCALL=0
timeStampAzure = NA
if(file.exists(timeStampAzureFile))
{
	timeStampAzure = readLines(timeStampAzureFile)
	tmAc = as.POSIXct(strptime(as.character(timeStampAzure), "%Y-%m-%d %H:%M:%S"))
	tmAc = tmAc - 1
	timeStampAzure = as.character(tmAc)
}

for(x in 1 : length(years))
{
  pathyrs = paste(path,years[x],sep="/")
	irrpathyears = paste(irrpath,years[x],sep="/")
  writepath2Gyr = paste(writepath2G,years[x],sep="/")
  checkdir(writepath2Gyr)
  writepath3Gyr = paste(writepath3G,years[x],sep="/")
  checkdir(writepath3Gyr)
  months = dir(pathyrs)
  for(y in 1 : length(months))
  {
    pathmonths = paste(pathyrs,months[y],sep="/")
    writepath2Gmon = paste(writepath2Gyr,months[y],sep="/")
		irrpathmonth = paste(irrpathyears,months[y],sep="/")
		daysirr = dir(irrpathmonth)
    checkdir(writepath2Gmon)
    writepath3Gfinal = paste(writepath3Gyr,"/[SG-007X] ",months[y],".txt",sep="")
    pathdays = pathmonths
    writepath2Gdays = writepath2Gmon
    days = dir(pathdays)
    temp = unlist(strsplit(days[1]," "))
    temp = as.Date(substr(temp[2],1,10),"%Y-%m-%d")
      for(t in 1 : length(days))
      {
        if(ENDCALL==1)
					break
				{
          if(x == 1 && y == 1 && t == 1)
          {
            DOB = unlist(strsplit(days[t]," "))
            DOB = substr(DOB[2],1,10)
            DOB = as.Date(DOB,"%Y-%m-%d")
          }
          else if(x == length(years) && y == length(months) && t == length(days))
          {
            next
          }
        }
        DAYSALIVE = DAYSALIVE + 1
        writepath2Gfinal = paste(writepath2Gdays,"/",days[t],sep="")
        readpath = paste(pathdays,days[t],sep="/")
        df1 = secondGenData(readpath,writepath2Gfinal)
        thirdGenData(writepath2Gfinal,writepath3Gfinal,timeStampAzure)
				if(days[t] == stopDate)
				{
					ENDCALL = 1
				}
      }
			if(ENDCALL==1)
				break
    }
		if(ENDCALL==1)
			break
}
print('Historical Analysis Done')
