rm(list = ls())
TIMESTAMPSALARM = NULL
#TIMESTAMPSALARM2 = NULL
METERCALLED = 0
ltcutoff = .001
CABLOSSTOPRINT = 0
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
	seq1 = seq(from = 1,to = length(hr),by =2)
	seq2 = seq(from = 2,to = length(hr),by =2)
  min = as.numeric(hr[seq2]) 
  hr = as.numeric(hr[seq1]) * 60
  return((hr + min + 1))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

getPRData = function(df)
{
	gsi=PR1=PR2=NA
	date = as.character(df[1,1])
	yr = substr(date,1,4)
	mon=substr(date,1,7)
	date = substr(date,1,10)
	path = paste('/home/admin/Dropbox/Second Gen/[PH-006S]/',yr,"/",mon,"/[PH-006S] ",date,".txt",sep="")
	print(paste('Path reading GSI is from',path))
	if(file.exists(path))
	{
	print('File exists')
	dataread = read.table(path,header = T,sep = "\t",stringsAsFactors=F)
	gsi = as.numeric(dataread[1,3])
	PR1 = round(as.numeric(df[,12])*100/gsi,1)
	PR2 = round(as.numeric(df[,13])*100/gsi,1)
	}
	array2 = c(gsi,PR1)
	return(array2)
}

secondGenData = function(filepath,writefilepath)
{
  {
		if(METERCALLED == 1){
	 		TIMESTAMPSALARM <<- NULL}
#		else if(METERCALLED == 2){
#			TIMESTAMPSALARM2 <<- NULL}
	}
  dataread = read.table(filepath,header = T,sep = "\t",stringsAsFactors=F)
	GSITOT = TMODMEAN=TMODMEANSH=lasttime=lastread=Eac2=Eac1=RATIO=DA=totrowsmissing=date=YLD1=YLD2=PR1=PR2=NA
	if(ncol(dataread) > 47 && length(as.numeric(dataread[complete.cases(as.numeric(dataread[,48])),48])))
	GSITOT = round(sum(as.numeric(dataread[complete.cases(as.numeric(dataread[,48])),48]))/60000,2)
	if(ncol(dataread) > 47 && length(as.numeric(dataread[complete.cases(as.numeric(dataread[,49])),49])))
	TMODMEAN = round(mean(as.numeric(dataread[complete.cases(as.numeric(dataread[,49])),49])),1)
	dataread2 = dataread
	dataread = dataread2[complete.cases(as.numeric(dataread2[,39])),]
	if(nrow(dataread))
	{
	lasttime = as.character(dataread[nrow(dataread),1])
	lastread = as.character(round(as.numeric(dataread[nrow(dataread),39])/1000,3))
	Eac2 = format(round((as.numeric(dataread[nrow(dataread),39]) - as.numeric(dataread[1,39]))/1000,1),nsmall=1)
	}
	YLD2 = format(round(as.numeric(Eac2)/190.8,2),nsmall=2)
	dataread = dataread2[complete.cases(as.numeric(dataread2[,15])),]
	dataread = dataread[as.numeric(dataread[,15]) > 0,]
	Eac1 = 0
	if(nrow(dataread) > 1)
	{
   	Eac1 = format(round(sum(as.numeric(dataread[,15]))/60000,1),nsmall=1)
	}
	YLD1 = format(round(as.numeric(Eac1)/190.8,2),nsmall=2)
	RATIO = round(as.numeric(Eac1)*100/as.numeric(Eac2),3)
	dataread = dataread2
  DA = format(round(nrow(dataread)/14.4,1),nsmall=1)
  tdx = timetomins(substr(dataread[,1],12,16))
  dataread2 = dataread[tdx > 540,]
  tdx = tdx[tdx > 540]
  dataread2 = dataread2[tdx < 1020,]
	if(length(as.numeric(dataread2[complete.cases(as.numeric(dataread2[,49])),49])))
	TMODMEANSH = round(mean(as.numeric(dataread2[complete.cases(as.numeric(dataread2[,49])),49])),1)
	dataread2 = dataread2[complete.cases(as.numeric(dataread2[,15])),]
  missingfactor = 480 - nrow(dataread2)
  dataread2 = dataread2[as.numeric(dataread2[,15]) < ltcutoff,]
	if(length(dataread2[,1]) > 0)
	{
		#{
			if(METERCALLED == 1){
				TIMESTAMPSALARM <<- as.character(dataread2[,1])}
			#else if(METERCALLED == 2){
		 	# TIMESTAMPSALARM2 <<- as.character(dataread2[,1])}
		#}
	}
  totrowsmissing = format(round((missingfactor + nrow(dataread2))/4.8,1),nsmall=1)
	if(nrow(dataread))
	date = as.character(dataread[1,1],1,10)
	PR1=format(round(as.numeric(YLD1)*100/GSITOT,1),nsmall=1)
	PR2=format(round(as.numeric(YLD2)*100/GSITOT,1),nsmall=1)
	{
	  if(METERCALLED == 1)
	  {
      df = data.frame(Date = substr(as.character(date),1,10), SolarPowerMeth1 = as.numeric(Eac1),SolarPowerMeth2 = as.numeric(Eac2),
                  Ratio=RATIO,DA = DA,Downtime = totrowsmissing,
									LastTime = lasttime, LastRead = lastread,
								GSITot = GSITOT,TModMean=TMODMEAN,TModMeanSH=TMODMEANSH,
									Yield1=YLD1,Yield2=YLD2,
									PR1=PR1,PR2=PR2,
									stringsAsFactors = F)
    } 
	}
	PRData = getPRData(df)
	{
	  if(METERCALLED == 1)
	  {
      df = data.frame(Date = substr(as.character(date),1,10), SolarPowerMeth1 = as.numeric(Eac1),SolarPowerMeth2 = as.numeric(Eac2),
                  Ratio=RATIO,DA = DA,Downtime = totrowsmissing,
									LastTime = lasttime, LastRead = lastread,
								GSITot = GSITOT,TModMean=TMODMEAN,TModMeanSH=TMODMEANSH,
									Yield1=YLD1,Yield2=YLD2,
									PR1=PR1,PR2=PR2,
									GSIPH006S=PRData[1],
									PRPH006S=PRData[2],
									stringsAsFactors = F)
    } 
	}
	write.table(df,file = writefilepath,row.names = F,col.names = T,sep="\t",append = F)
  return(df)
}

thirdGenData = function(filepathm2,writefilepath)
{
  dataread1 = read.table(filepathm2,header = T,sep = "\t") #its correct dont change
  {
    if(file.exists(writefilepath))
    {
      write.table(dataread1,file = writefilepath,row.names = F,col.names = F,sep = "\t",append = T)
    }
    else
    {
      write.table(dataread1,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
    }
  }
}

