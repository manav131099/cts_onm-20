rm(list = ls())
require('compiler')
enableJIT(3)
errHandle = file('/home/admin/Logs/LogsIN068LHistorical.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
source('/home/admin/CODE/IN068LDigest/summaryFunctions.R')
RESETHISTORICAL=1
daysAlive = 0
reorderStnPaths = c(16,15,1,7,8,9,10,11,12,13,14,2,3,4,5,6)

if(RESETHISTORICAL)
{
  system("rm -R \"/home/admin/Dropbox/Second Gen/[IN-068L]\"")
  system("rm -R \"/home/admin/Dropbox/Third Gen/[IN-068L]\"")
  system("rm -R /home/admin/Dropbox/Fourth_Gen/[IN-068L]")
}

path = "/home/admin/Dropbox/Gen 1 Data/[IN-068L]"
pathwrite2G = "/home/admin/Dropbox/Second Gen/[IN-068L]"
pathwrite3G = "/home/admin/Dropbox/Third Gen/[IN-068L]"
pathwrite4G = "/home/admin/Dropbox/Fourth_Gen/[IN-068L]"


checkdir(pathwrite2G)
checkdir(pathwrite3G)
checkdir(pathwrite4G)
years = dir(path)
for(x in 1 : length(years))
{
  pathyr = paste(path,years[x],sep="/")
  pathwriteyr = paste(pathwrite2G,years[x],sep="/")
  checkdir(pathwriteyr)
  months = dir(pathyr)
  if(!length(months))
    next
  for(y in 1 : length(months))
  {
    pathmon = paste(pathyr,months[y],sep="/")
    pathwritemon = paste(pathwriteyr,months[y],sep="/")
    checkdir(pathwritemon)
    stns = dir(pathmon)
    if(!length(stns))
      next
    stns = stns[reorderStnPaths]
    print(length(stns))
    for(z in 1 : length(stns))
    {
      type = 1
      pathstn = paste(pathmon,stns[z],sep="/")
      if(stns[z] == "MFM_1")
        type = 0
      if(stns[z] == "WMS_1")
        type = 2
      pathwritestn = paste(pathwritemon,stns[z],sep="/")
      checkdir(pathwritestn)
      days = dir(pathstn)
      if(!length(days))
        next
      for(t in 1 : length(days))
      {
        if(z == 1)
          daysAlive = daysAlive + 1
        pathread = paste(pathstn,days[t],sep="/")
        pathwritefile = paste(pathwritestn,days[t],sep="/")
        if(RESETHISTORICAL || !file.exists(pathwritefile))
        {
          secondGenData(pathread,pathwritefile,type)
          print(paste(days[t],"Done"))
        }
      }
    }
  }
}
sink()
