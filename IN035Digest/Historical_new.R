rm(list = ls())
require('compiler')
enableJIT(3)
errHandle = file('/home/admin/Logs/LogsIN035Historical.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
source('/home/admin/CODE/IN035Digest/summaryFunctions_new.R')
RESETHISTORICAL=1
daysAlive = 0
reorderStnPaths = c(8,4,5,6,7)

if(RESETHISTORICAL)
{
system('rm -R /home/admin/Dropbox/FlexiMC_Data/Second_Gen/[IN-035C]')
system('rm -R /home/admin/Dropbox/FlexiMC_Data/Azure_Second_Gen/[IN-035C]')
system('rm -R /home/admin/Dropbox/FlexiMC_Data/Third_Gen/[IN-035C]')
system('rm -R /home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/[IN-035C]')
system('rm -R /home/admin/Dropbox/FlexiMC_Data/Azure_Fourth_Gen/[IN-035C]')
}

path = "/home/admin/Dropbox/FlexiMC_Data/Gen1_Data/[IN-035C]"
pathwrite2G = "/home/admin/Dropbox/FlexiMC_Data/Second_Gen/[IN-035C]"
pathwrite3G = "/home/admin/Dropbox/FlexiMC_Data/Third_Gen/[IN-035C]"
pathwrite4G = "/home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/[IN-035C]"

checkdir(pathwrite2G)
checkdir(pathwrite3G)
checkdir(pathwrite4G)
years = dir(path)
for(x in 3 : length(years))
{
	pathyr = paste(path,years[x],sep="/")
	pathwriteyr = paste(pathwrite2G,years[x],sep="/")
	checkdir(pathwriteyr)
	months = dir(pathyr)
	if(!length(months))
		next
	for(y in 6 : length(months))
	{
		pathmon = paste(pathyr,months[y],sep="/")
		pathwritemon = paste(pathwriteyr,months[y],sep="/")
		checkdir(pathwritemon)
		stns = dir(pathmon)
		if(!length(stns))
			next
		stns = stns[reorderStnPaths]
   
		for(z in 1 : length(stns))
		{
			type = 1
			pathstn = paste(pathmon,stns[z],sep="/")
			if(grepl("OG",stns[z]))
				type = 0
			pathwritestn = paste(pathwritemon,stns[z],sep="/")
			checkdir(pathwritestn)
			days = dir(pathstn)
			if(!length(days))
				next
			
      if (type==0){
        for(t in 22 : length(days))
  			{
          if(z == 1)
  					daysAlive = daysAlive + 1
  				pathread = paste(pathstn,days[t],sep="/")
  				pathwritefile = paste(pathwritestn,days[t],sep="/")
          if(RESETHISTORICAL || !file.exists(pathwritefile))
  				{
  					secondGenData(pathread,pathwritefile,type)
  					print(paste(days[t],"Done"))
  				}
  			}
      }
      
      if (type==1){
        for(t in 25 : length(days))
  			{
          if(z == 1)
  					daysAlive = daysAlive + 1
  				pathread = paste(pathstn,days[t],sep="/")
  				pathwritefile = paste(pathwritestn,days[t],sep="/")
          if(RESETHISTORICAL || !file.exists(pathwritefile))
  				{
  					secondGenData(pathread,pathwritefile,type)
  					print(paste(days[t],"Done"))
  				}
  			}
      }
		}
	}
}
sink()
