require('compiler')

checkdirUnOp = function(path)
{
if(!file.exists(path))
{
	dir.create(path)
}
}

checkdir = cmpfun(checkdirUnOp)

INSTCAP = c(1209,1170,1131)
INSTCAPM = c(3510,3510)
NOMETERS=2
NOINVS=3
getGTIDataUnOp = function(date)
{
	yr = substr(date,1,4)
	yrmon = substr(date,1,7)
	filename = paste("[IN-035C]-OG-",date,".txt",sep="")
	path = "/home/admin/Dropbox/FlexiMC_Data/Second_Gen/[IN-035C]"
	path2 = "/home/admin/Dropbox/FlexiMC_Data/Gen1_Data/[IN-035C]"
	pathRead = paste(path,yr,yrmon,"OG",filename,sep="/")
	pathRead2= paste(path2,yr,yrmon,"OG",filename,sep="/")
	#GTIGreater20=NA
	#GTI = GHI = NA
	#if(file.exists(pathRead))
	#{
  #	dataread = read.table(pathRead,sep="\t",header = T)
	#	if(nrow(dataread) > 0)
	#	{
	#		GTI = as.numeric(dataread[1,3])
	#		GHI = as.numeric(dataread[1,8])
	#	}
	#}
	#if(file.exists(pathRead2))#return all timestamps with irr>20
	#{
	#  dataread = read.table(pathRead2,sep="\t",header = T,stringsAsFactors = F)
	#  dataread=dataread[complete.cases(dataread[21]),]
	#  GTIGreater20=dataread[dataread[,21]>20,1]
	#}
	#return(c(GTI,GHI,GTIGreater20))
}

#getGTIData = cmpfun(getGTIDataUnOp)

timetominsUnOp = function(time)
{
	hr = as.numeric(substr(time,12,13))
	min = as.numeric(substr(time,15,16))
	bucket =((hr*60) + min + 1)
	return(bucket)
}

timetomins = cmpfun(timetominsUnOp)

secondGenDataUnOp = function(pathread, pathwrite,type)
{
	dataread = try(read.table(pathread,sep="\t",header = T),silent = T)
	if(class(dataread) == "try-error")
	{
		print(paste('pathread error',pathread))
		date=DA=Eac1=Eac3=Eac2=Yld1=Yld2=LR=Tamb=Tmod=GTI=LT=TModSH=TambSH=GHI=WS=PR1G=PR2G=GA=PA=IA=NA
		{
		if(type == 0)
			data = data.frame(Date = date,DA = DA,Eac1 = Eac1, Eac2 = Eac2, Yld1 = Yld1,
			Yld2 = Yld2, PR1=PR1,PR2=PR2, LastRead = LR, LastTime = LT,PR1GHI=PR1G,PR2GHI=PR2G, GA=GA, PA=PA)
		else if(type == 1)
			data = data.frame(Date = date,DA = DA,Eac1 = Eac1, EacAC = Eac2, EacDC = Eac3,
			Yld1 = Yld1, Yld2 = Yld2,LastRead = LR, LastTime = LT, IA=IA)
		}
		write.table(data,file=pathwrite,row.names =F,col.names = T,sep="\t",append=F)
		thirdGenData(pathwrite)
		fourthGenData(pathwrite)
		return()	
	}
	date = substr(dataread[1,1],1,10)
	DA = format(round((nrow(dataread)/2.88),1),nsmall=1)
	{
		if(type == 0)
		{
			idxPower = 32
			idxEnergy = 66
			Eac1 = Eac2 = Yld1 = Yld2 = LR = LT =PR1 = PR2 = PR1G=PR2G=GA=PA=NA
			idxmtchdt = 1
			#Add condition to ensure power isn't greater than 3500
			#Add condition for energy -- abs (rown[x] - row[x-1] ) 1000 -- think of mean - actual value
			if(grepl("ICOG",pathread))
				idxmtchdt = 2
			

			dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxEnergy])),idxEnergy])
			dataInt2 = as.character(dataread[complete.cases(as.numeric(dataread[,idxEnergy])),1])
				
			dataIntTr = round(dataInt/1000000,0)
			dataIntTr2 = unique(dataIntTr)
			mode = dataIntTr2[which.max(tabulate(match(dataIntTr, dataIntTr2)))]
			dataInt = dataInt[abs(dataIntTr - mode) <= 100]
			dataInt2 = dataInt2[abs(dataIntTr - mode) <= 100]

			if(length(dataInt))
			{
				Eac2 = round((abs(dataInt[length(dataInt)] - dataInt[1])),2)
				LR = dataInt[length(dataInt)]
				LT = dataInt2[length(dataInt2)]
			}
			dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxPower])),idxPower])
			dataInt = dataInt[abs(dataInt) < 1000000]
			if(length(dataInt))
				Eac1 = format(round((sum(dataInt)/12),2),nsmall=2)
			
			Yld1 = round(as.numeric(Eac1)/INSTCAPM[idxmtchdt],2)
			Yld2 = round(as.numeric(Eac2)/INSTCAPM[idxmtchdt],2)
			data = data.frame(Date = date,DA = DA,Eac1 = Eac1, Eac2 = Eac2, Yld1 = Yld1,
			Yld2 = Yld2, PR1=PR1, PR2 = PR2, LastRead = LR,LastTime = LT,PR1GHI = PR1G,
			PR2GHI=PR2G, GA=GA, PA=PA)
		}
		else if(type == 1)
		{
			idxEnergy = 31
			idxPowerAC = 15
			idxPowerDC = 21
			Eac1 = Eac2 = Eac3 = LR = LT= IA = NA
			idxmtchdt  = 3
			if(grepl("Inverter_1",pathread))
				idxmtchdt = 1
			if(grepl("Inverter_2",pathread))
				idxmtchdt = 2
			
			dataInt = dataread[(timetomins(dataread[,1]) < 1230),]
			dataInt2 = as.character(dataInt[complete.cases(as.numeric(dataInt[,idxEnergy])),1])
			dataInt = as.numeric(dataInt[complete.cases(as.numeric(dataInt[,idxEnergy])),idxEnergy])
			if(length(dataInt) && (!dataInt[length(dataInt)]))
			{
				dataIntnonZero = as.numeric(dataInt[as.numeric(dataInt) !=0 ])
				if(length(dataIntnonZero))
					dataInt[length(dataInt)] = dataIntnonZero[length(dataIntnonZero)]
			}
			
			dataIntTr = round(dataInt/1000000,0)
			dataIntTr2 = unique(dataIntTr)
			mode = dataIntTr2[which.max(tabulate(match(dataIntTr, dataIntTr2)))]
			dataInt = dataInt[abs(dataIntTr - mode) <= 1]
			dataInt2 = dataInt2[abs(dataIntTr - mode) <= 1]
			
			if(length(dataInt))
			{
				Eac2 = round((abs(dataInt[length(dataInt)] - dataInt[1])),2)
				LR = dataInt[length(dataInt)]
				LT = dataInt2[length(dataInt2)]
			}
			dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxPowerAC])),idxPowerAC])
			dataInt = dataInt[abs(dataInt) < 1000000]
			if(length(dataInt))
				Eac1 = format(round((sum(dataInt)/12000),2),nsmall=2)
			
			dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxPowerDC])),idxPowerDC])
			dataInt = dataInt[abs(dataInt) < 1000000]
			if(length(dataInt))
				Eac3 = format(round((sum(dataInt)/12000),2),nsmall=2)

			Yld1 = round(as.numeric(Eac1)/INSTCAP[idxmtchdt],2)
			Yld2 = round(as.numeric(Eac2)/INSTCAP[idxmtchdt],2)
			
			data = data.frame(Date = date,DA = DA,Eac1 = Eac1, EacAC = Eac2, EacDC = Eac3,
			Yld1 = Yld1, Yld2 = Yld2, LastRead = LR, LastTime = LT, IA = IA)
		}
   
	}
	write.table(data,file=pathwrite,row.names =F,col.names = T,sep="\t",append=F)
	print("done writing")
	thirdGenData(pathwrite)
	fourthGenData(pathwrite)
}

secondGenData = cmpfun(secondGenDataUnOp)

thirdGenDataUnOp = function(path2G)
{
		stnnames = unlist(strsplit(path2G,"/"))
		path3G = paste("/home/admin/Dropbox/FlexiMC_Data/Third_Gen",stnnames[7],sep="/")
		checkdir(path3G)
		path3GStn = paste(path3G,stnnames[10],sep="/")
		checkdir(path3GStn)
		pathwrite3GFinal = paste(path3GStn,paste(substr(stnnames[9],1,18),".txt",sep=""),sep="/")
		dataread = read.table(path2G,header = T,sep ="\t")
		{
			if(!file.exists(pathwrite3GFinal))
				write.table(dataread,file=pathwrite3GFinal,row.names=F,col.names=T,append=F,sep="\t")
			else
				write.table(dataread,file=pathwrite3GFinal,row.names=F,col.names=F,append=T,sep="\t")
		}
		dataread = read.table(pathwrite3GFinal,header = T,sep="\t")
		tm = as.character(dataread[,1])
		tmuniq = unique(tm)
		# The idea here is update third-gen with the most recent readings. So if 
		# there are multiple calls, only take the latest row for the day and 
		# slot that into the third-gen data
		if(length(tm) > length(tmuniq))
		{
			dataread = dataread[-(nrow(dataread)-1),]
		}
		write.table(dataread,file=pathwrite3GFinal,row.names=F,col.names=T,append=F,sep="\t")
}

thirdGenData = cmpfun(thirdGenDataUnOp)

fourthGenDataUnOp = function(path2G)
{
	order = c("OG","ICOG","Inverter_1","Inverter_2", "Inverter_3")
	CONSTANTLEN = c(unlist(rep(13,NOMETERS)),unlist(rep(9,NOINVS))) # No of columns for each file excluding date
	stnnames = unlist(strsplit(path2G,"/"))
	rowtemplate = unlist(rep(NA,sum(CONSTANTLEN)+1))
	path4G = paste("/home/admin/Dropbox/FlexiMC_Data/Fourth_Gen",stnnames[7],sep="/")
	checkdir(path4G)
	dataread = read.table(path2G,header = T,sep ="\t")
	pathfinal = paste(path4G,paste(stnnames[7],"-lifetime.txt",sep=""),sep="/")
	idxmtch = match(stnnames[10],order)
	start = 2
	if(idxmtch > 1)
	{
		start = 2 + sum(CONSTANTLEN[1:(idxmtch -1)])
	}
	end = start + CONSTANTLEN[idxmtch] - 1
	idxuse = start : end
	colnames2G = colnames(dataread)
	colnames2G = colnames2G[-1]
	colnames2G = paste(stnnames[10],colnames2G,sep="-")

	{
		if(!file.exists(pathfinal))
		{
			colnames = rep("Date",length(rowtemplate))
			colnames[idxuse] = colnames2G
			rowtemplate[idxuse] = dataread[1,(2:ncol(dataread))]
			rowtemplate[1] = as.character(dataread[1,1])
			dataExist = data.frame(rowtemplate)
			colnames(dataExist) = colnames
			write.table(dataExist,file=pathfinal,row.names =F,col.names=T,sep="\t",append=F)
		}
		else
		{
			dataExist = read.table(pathfinal,sep="\t",header=T)
			colnames = colnames(dataExist)
			colnames[idxuse] = colnames2G
			dates = as.character(dataExist[,1])
			idxmtchdt = match(as.character(dataread[,1]),dates)
			if(is.finite(idxmtchdt))
			{
				tmp = dataExist[idxmtchdt,]
				tmp[idxuse] = dataread[1,2:ncol(dataread)]
				dataExist[idxmtchdt,] = tmp
				colnames(dataExist) = colnames
				write.table(dataExist,file=pathfinal,row.names =F,col.names=T,sep="\t",append=F)
			}
			else
			{
				rowtemplate = unlist(rowtemplate)
				rowtemplate[c(1,idxuse)] = dataread[1,]
				dataExist = data.frame(rowtemplate)
				colnames(dataExist) = colnames
				write.table(dataExist,file=pathfinal,row.names =F,col.names=F,sep="\t",append=T)
			}
		}
	}

	print(paste("4G Done",path2G))
}

fourthGenData = cmpfun(fourthGenDataUnOp)
