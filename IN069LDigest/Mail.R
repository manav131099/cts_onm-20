rm(list=ls())
require('compiler')
enableJIT(3)
errHandle = file('/home/admin/Logs/LogsIN069LMail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

source('/home/admin/CODE/IN069LDigest/summaryFunctions.R')
source('/home/admin/CODE/common/math.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
RESETHISTORICAL=0
daysAlive = 0
reorderStnPaths = c(30,23,24,25,1,12,16,17,18,19,20,21,22,2,3,4,5,6,7,8,9,10,11,13,14)

source('/home/admin/CODE/MasterMail/timestamp.R')
require('mailR')
source('/home/admin/CODE/IN069LDigest/aggregateInfo.R')


MYLDN = vector()
MYLDS = vector()
MYLD_arr = vector()

METERNICKNAMES = c("WMS","MFM_1 (Unit 2)","MFM_2 (COlony Area)","MFM_3 (Unit 1)","INV-1","INV-2","INV-3","INV-4","INV-5","INV-6","INV-7","INV-8","INV-9","INV-10","INV-11","INV-12","INV-13","INV-14","INV-15","INV-16","INV-5 (UNIT-1)","INV-4 (UNIT-1)","INV-1 (UNIT-2)","INV-2 (UNIT-2)","INV-3 (UNIT-2)")

METERACNAMES = c("WMS_2","MFM_1","MFM_2","MFM_3","INVERTER_1","INVERTER_2","INVERTER_3","INVERTER_4","INVERTER_5","INVERTER_6","INVERTER_7","INVERTER_8","INVERTER_9","INVERTER_10","INVERTER_11","INVERTER_12","INVERTER_13","INVERTER_14","INVERTER_15","INVERTER_16","INVERTER_17","INVERTER_18","INVERTER_19","INVERTER_20","INVERTER_21")

checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

DOB = "22-04-2020"
DOB2="2020-04-22"
DOB2=as.Date(DOB2)
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
recipients = c('operationsWestIN@cleantechsolar.com','om-it-digest@cleantechsolar.com')
pwd = 'CTS&*(789'
referenceDays = NA
extractDaysOnly = function(days)
{
	if(length(days!=0))
	{
	seq = seq(from=1,to=length(days)*6,by=6)
	days = unlist(strsplit(days,"-"))
	days = paste(days[seq+3],days[seq+4],days[seq+5],sep="-")
	return(days)
	}
}
analyseDays = function(days,ref)
{
  if(length(days) == length(ref))
    return(days)
  daysret = unlist(rep(NA,length(ref)))
  if(length(days) == 0)
    return(daysret)
  days2 = extractDaysOnly(days)
  idxmtch = match(days2,ref)
  idxmtch2 = idxmtch[complete.cases(idxmtch)]
  if(length(idxmtch) != length(idxmtch2))
  {
    print(".................Missmatch..................")
    print(days2)
    print("#########")
    print(ref)
    print("............................................")
    return(days)
  }
  if(is.finite(idxmtch))
  {
    daysret[idxmtch] = as.character(days)
    if(!(is.na(daysret[length(daysret)])))
      return(daysret)
  }	
  return(days)
}
performBackWalkCheck = function(day)
{
  path = "/home/admin/Dropbox/Gen 1 Data/[IN-069L]"
  retval = 0
  yr = substr(day,1,4)
  yrmon = substr(day,1,7)
  pathprobe = paste(path,yr,yrmon,sep="/")
  stns = dir(pathprobe)
  print(stns)
  idxday = c()
  for(t in 1 : length(stns))
  {
    pathdays = paste(pathprobe,stns[t],sep="/")
    print(pathdays)
    days = dir(pathdays)
    dayMtch = days[grepl(day,days)]
    if(length(dayMtch))
    {
      idxday[t] = match(dayMtch,days)
    }
    print(paste("Match for",day,"is",idxday[t]))
  }
  idxmtch = match(NA,idxday[t])
  if(length(unique(idxday))==1 || length(idxmtch) < 5)
  {
    print('All days present ')
    retval = 1
  }
  return(retval)
}
sendMail= function(pathall)
{
  path = pathall[t]
  dataread = read.table(path,header = T,sep="\t")
  currday = as.character(dataread[1,1])
  Cur=as.Date(currday)
  filenams = c()
  body = ""
  body = paste(body,"Site Name: Prism Cement Satna",sep="")
  body = paste(body,"\n\nLocation: Satna, India")
  body = paste(body,"\n\nO&M Code: IN-069")
  body = paste(body,"\n\nSystem Size [kWp]:",INSTCAP)
  body = paste(body,"\n\nNumber of Energy Meters: 3")
  body = paste(body,"\n\nModule Brand / Model / Nos: Trina Solar / 330 W / TBD")
  body = paste(body,"\n\nInverter Brand / Model / Nos: Sungrow / Tmeic / 60 kW / 2.5 MW / 16 / 5")
  body = paste(body,"\n\nSite COD: 2020-04-22")
  body = paste(body,"\n\nSystem age [days]:",as.character((as.numeric(Cur-DOB2))))
  body = paste(body,"\n\nSystem age [years]:",as.character(round((as.numeric(Cur-DOB2))/365,1)))
  body = paste(body,"\n")
  bodyac = body
  body = ""
  TOTALGENCALC = 0
  yr = substr(Cur,1,4)
  yrmon = substr(Cur,1,7)
  
  
  path3 = "/home/admin/Dropbox/Gen 1 Data/[IN-069L]"
  file3 = paste("[IN-069L]-WMS1-",Cur,".txt",sep="")
  pathRead3 = paste(path3,yr,yrmon,"WMS_1",file3,sep="/")

  GTI = DNI = NA
  
  if(file.exists(pathRead3))
  {
    data3 = read.table(pathRead3, sep = "\t", header = T)
    
    if(nrow(data3) > 0)
    {
      GTI_pyr = sum(data3[, 'OTI_avg'], na.rm = TRUE)
    }
    else
    {
      GTI_pyr= 'NA'
      
    }
    GTI_pyr = round(GTI_pyr/12000,2)  
  }
  MYLD = c()
  
  acname = METERNICKNAMES
  noInverters = NOINVS
  globMtYld = globMtVal = globMtname = c()
  idxglobMtYld=1
  InvReadings = unlist(rep(NA,noInverters))
  InvAvail = unlist(rep(NA,noInverters))
  for(t in 1 : length(pathall))
  {
    type = 0
    meteridx = t
    if(grepl("WMS",pathall[t])) 
      type = 1
    else if(grepl("MFM",pathall[t]))
      type=1
    else
      type = 0
    if( t > (1+NOMETERS))
    {
      splitpath = unlist(strsplit(pathall[t],"INVERTER_"))
      if(length(splitpath)>1)
      {
        meteridx = unlist(strsplit(splitpath[2],"/"))
        meteridx = as.numeric(meteridx[1])
      }
    }
    path = pathall[t]
    dataread = read.table(path,header = T,sep="\t")
    currday = as.character(dataread[1,1])
    if(type==0)
    {
      filenams[t] = paste(currday,"-",METERNICKNAMES[meteridx+2],".txt",sep="")
    }
    else if(type==1 && grepl("WMS",pathall[t]))
    {
      filenams[t] = paste(currday,"-",'WMS',".txt",sep="")
    }
    else
    {
      filenams[t] = paste(currday,"-",'MFM',".txt",sep="") 
    }
    print("---------------------")
    print(path)
    print(filenams[t])
    print("---------------------")
    if(type == 1)
    {
      body = paste(body,"\n\n________________________________________________\n\n")
      body = paste(body,currday,acname[t])
      body = paste(body,"\n________________________________________________\n\n")
      body = paste(body,"DA [%]:",as.character(dataread[1,2]),"\n\n")
      {
        if(grepl("MFM",pathall[t]))
        {
          print("PRINTING MFM")
          if(grepl("MFM_1",pathall[t])){
            body = paste(body,"System Size [kWp]:",INSTCAPM[1],"\n\n")
          }else if(grepl("MFM_2",pathall[t])){
            body = paste(body,"System Size [kWp]:",INSTCAPM[2],"\n\n")
          }else if(grepl("MFM_3",pathall[t])){
            body = paste(body,"System Size [kWp]:",INSTCAPM[3],"\n\n")
          }
          body = paste(body,"EAC method-1 (Pac) [kWh]:",as.character(format(round(dataread[1,3],1), nsmall = 1)),"\n\n")
          body = paste(body,"EAC method-2 (Eac) [kWh]:",as.character(format(round(dataread[1,4],1), nsmall = 1)),"\n\n")
          TOTALGENCALC = TOTALGENCALC + as.numeric(dataread[1,4])
          body = paste(body,"Yield-1 [kWh/kWp]:",as.character(format(round(dataread[1,5],2), nsmall = 2)),"\n\n")
          yld1 = dataread[1,5]
          body = paste(body,"Yield-2 [kWh/kWp]:",as.character(format(round(dataread[1,6],2), nsmall = 2)),"\n\n")
          yld2 = dataread[1,6]
          globMtYld[idxglobMtYld] = as.numeric(dataread[1,6])
          globMtVal[idxglobMtYld] = as.numeric(dataread[1,4])
          #globMtname[idxglobMtYld] = acnames[idx]
          idxglobMtYld = idxglobMtYld + 1
           if(grepl("MFM_2",pathall[t])){
          body = paste(body,"PR-1 (RT1) [%]:",round(as.numeric(yld1*100/GTI),1),"\n\n")
          body = paste(body,"PR-2 (RT1) [%]:",round(as.numeric(yld2*100/GTI),1),"\n\n")
          }else{
            body = paste(body,"PR-1 (Pyranometer) [%]:",round(as.numeric(yld1*100/GTI_pyr),1),"\n\n")
            body = paste(body,"PR-2 (Pyranometer) [%]:",round(as.numeric(yld2*100/GTI_pyr),1),"\n\n")
          }
          body = paste(body,"Last recorded value [kWh]:",as.character(format(round(dataread[1,9],1), nsmall = 1)),"\n\n")
          body = paste(body,"Last recorded time:",as.character(dataread[1,10]),"\n\n")
          body = paste(body,"Grid Availability [%]:",as.character(dataread[1,13]),"\n\n")
          body = paste(body,"Plant Availability [%]:",as.character(dataread[1,14]))
        }
        else
        {
          GTI = dataread[1,3]
          body = paste(body,"GTI (RT1) [kWh/m^2]:",as.character(format(round(dataread[1,3],2), nsmall = 2)),"\n\n")
          body = paste(body,"GTI from Pyranometer [kWh/m^2]:",GTI_pyr,"\n\n")
          body = paste(body,"Avg Tmod [C]:",as.character(format(round(dataread[1,5],1), nsmall = 1)),"\n\n")
          body = paste(body,"Avg Tmod solar hours [C]:",as.character(format(round(dataread[1,7],1), nsmall = 1)))
        }
      }
      next
    }
    InvReadings[meteridx] = as.numeric(dataread[1,7])
    InvAvail[meteridx]= as.numeric(dataread[1,10])
    MYLD[(meteridx)] = as.numeric(dataread[1,7])
  }
  body = paste(body,"\n\n________________________________________________\n\n")
  body = paste(body,currday,"Inverters")
  body = paste(body,"\n________________________________________________")
  MYLDCPY = MYLD
  MYLD_arr = c(MYLD)
  MYLDN = c(MYLDN, MYLD_arr[1], MYLD_arr[2], MYLD_arr[3], MYLD_arr[4], MYLD_arr[5],MYLD_arr[6], MYLD_arr[7], MYLD_arr[8],MYLD_arr[9], MYLD_arr[10], MYLD_arr[11], MYLD_arr[12], MYLD_arr[13],MYLD_arr[14], MYLD_arr[15], MYLD_arr[16])
  
  print(MYLDCPY)
  if(length(MYLD))
  {
    MYLD = MYLD[complete.cases(MYLD)]
    MYLDN = MYLDN[complete.cases(MYLDN)]
  }
  addwarn = 0
  sddev = covar = sddev_north = NA
  if(length(MYLD))
  {
    addwarn = 1
    sddev = round(sdp(MYLD),1)
    meanyld = mean(MYLD)
    covar = round((sdp(MYLD)*100/meanyld),1)
    
    sddev_north = round(sdp(MYLDN),2)
    meanyld_north = mean(MYLDN)
    covar_north = round((sdp(MYLDN)*100/meanyld_north),1)
  }
  MYLD = MYLDCPY
  for(t in 1 : noInverters)
  {
    body = paste(body,"\n\n Yield ",acname[(t+1+NOMETERS)],": ",as.character(InvReadings[t]),sep="")
    if( addwarn && is.finite(covar) && covar > 5 &&
        (!is.finite(InvReadings[t]) || (abs(InvReadings[t] - meanyld) > 2*sddev)))
    {
      body = paste(body,">>> Inverter outside range")
    }
  }
  body = paste(body,"\n\nAll Colony Area Inverters: Stdev/COV Yields: ",sddev_north," / ",covar_north,"%",sep="")
  body = paste(body,"\n\n All Inverters: Stdev/COV Yields: ",sddev," / ",covar,"%",sep="")
  
  #for(t in 1 : noInverters)
  #{
  #  body = paste(body,"\n\n Inverter Availability ",acname[(t+NOMETERS)]," [%]: ",as.character(InvAvail[t]),sep="")
  #}
  for(t in 1 : NOMETERS)
  {
    bodyac = paste(bodyac,"Yield",acname[(t+1)],"[kWh/kWp]:",as.character(format(round(globMtYld[t],2), nsmall = 2,)),"\n\n")
  }
  sdm =round(sdp(globMtYld),3)
  covarm = round(sdm*100/mean(globMtYld),1)
  bodyac = paste(bodyac,"Stdev/COV Yields:",as.character(format(round(sdm,2), nsmall = 2)),"/",as.character(format(round(covarm,1), nsmall = 1)),"[%]")
  bodyac = paste(bodyac,"\n\n________________________________________________\n\n")
  bodyac = paste(bodyac,currday,"FULL SITE")
  bodyac = paste(bodyac,"\n________________________________________________\n")
  bodyac = paste(bodyac,"\n\nEnergy Generated FULL SITE [kWh]:",sum(globMtVal))
  bodyac = paste(bodyac,"\n\nYield FULL SITE [kWh/kWp]:",
                 round(sum(globMtVal)/sum(INSTCAPM),2))
  bodyac = paste(bodyac,"\n\nGTI [kWh/m2]:",GTI_pyr)
  bodyac = paste(bodyac,"\n\nPerformance Ratio FULL SITE [%]:",
                 round(sum(globMtVal)/(sum(INSTCAPM)*GTI_pyr)*100,1))
  body = gsub("\n ","\n",body)
  body = paste(bodyac,body,sep="")

  #Generating Graphs
  graph_command1 = paste("python3 /home/admin/CODE/IN069LDigest/Pyr_vs_Si_Graph.py", currday, sep=" ")
	system(graph_command1, wait=TRUE)
	graph_path1 = paste('/home/admin/Graphs/Graph_Output/IN-069/[IN-069] Graph (Beta) ', currday, " - Pyranometer PR vs Silicon Sensor PR.pdf", sep="")
  graph_path2 = paste('/home/admin/Graphs/Graph_Output/IN-069/[IN-069] Graph (Beta) ', currday, " - PR vs GHI Pyranometer.pdf", sep="")
  graph_path3 = paste('/home/admin/Graphs/Graph_Output/IN-069/[IN-069] Graph (Beta) ', currday, " - PR vs GHI Silicon Sensor.pdf", sep="")
	graph_extract_path1 = paste('/home/admin/Graphs/Graph_Extract/IN-069/[IN-069] Graph ', currday, " - Pyranometer vs Silicon Sensor.txt", sep="")
  filestosendpath = c(graph_path1, graph_path2, graph_path3, graph_extract_path1)
  
  graph_command2 = paste('python3 /home/admin/CODE/EmailGraphs/Inverter_CoV_Graph.py "IN-069"', currday, 5, '2020-04-22', '" "', '"Inverter CoV"', sep=" ")
  system(graph_command2)
  graph_command3 = paste('python3 /home/admin/CODE/EmailGraphs/Grid_Availability_Graph_Azure_COD.py "IN-069"', currday, '2020-04-22', sep=" ")
  system(graph_command3)
  graph_command4 = paste("Rscript /home/admin/CODE/EmailGraphs/PR_Graph_Azure.R", "IN-069", 73.3, 0.008, "2020-04-22", currday, sep=" ")
  system(graph_command4)
  graph_command5 = paste('python3 /home/admin/CODE/EmailGraphs/Meter_CoV_Graph.py "IN-069"', currday, 5, sep=" ")
  system(graph_command5)
  
  #Graph Paths
  graphs = c('PR Evolution', 'Inverter CoV', 'Meter CoV', 'Grid Availability', 'Plant Availability', 'Data Availability', 'System Availability')
  for(g in 1 : 7){
    graph_path = paste('/home/admin/Graphs/Graph_Output/IN-069/[IN-069] Graph ', currday, ' - ', graphs[g], '.pdf', sep="")
    filestosendpath = c(filestosendpath, graph_path)
  }

  #Graph Extract Paths
  graph_extracts = c('PR Evolution', 'Inverter CoV', 'Meter CoV', 'Master Extract Daily', 'Master Extract Monthly')
  for(g in 1 : 5){
    if(g<4){
      graph_extract_path = paste('/home/admin/Graphs/Graph_Extract/IN-069/[IN-069] Graph ', currday, ' - ', graph_extracts[g], '.txt', sep="")
    }else{
      graph_extract_path = paste('/home/admin/Graphs/Graph_Extract/IN-069/[IN-069]', ' - ', graph_extracts[g], '.txt', sep="")
    }
    filestosendpath = c(filestosendpath, graph_extract_path)
  }
  
  filestosendpath = c(filestosendpath, pathall)
  
  send.mail(from = sender,
            to = recipients,
            subject = paste("Station [IN-069L] Digest",currday),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = filestosendpath,
            debug = F)
  recordTimeMaster("IN-069L","Mail",currday)
}


path = "/home/admin/Dropbox/Gen 1 Data/[IN-069L]"
path2G = '/home/admin/Dropbox/Second Gen/[IN-069L]'
path3G = '/home/admin/Dropbox/Third Gen/[IN-069L]'
path4G = '/home/admin/Dropbox/Fourth_Gen/[IN-069L]'


if(RESETHISTORICAL)
{
  command = paste("rm -rf",path2G)
  system(command)
  command = paste("rm -rf",path3G)
  system(command)
  command = paste("rm -rf",path4G)
  system(command)
}

checkdir(path2G)
checkdir(path3G)

years = dir(path)
stnnickName2 = "IN-069L"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
stopDate = paste("[",stnnickName2,"]-I21-",lastdatemail,".txt",sep="")
print(paste('StopDate is',stopDate))
ENDCALL=0
for(x in 1 : length(years))
{
  path2Gyr = paste(path2G,years[x],sep = "/")
  pathyr = paste(path,years[x],sep="/")
  checkdir(path2Gyr)
  months = dir(pathyr)
  for(y in 1 : length(months))
  {
    path2Gmon = paste(path2Gyr,months[y],sep = "/")
    pathmon = paste(pathyr,months[y],sep="/")
    checkdir(path2Gmon)
    stns = dir(pathmon)
    stns = stns[reorderStnPaths]
    dunmun = 0
    for(t in 1 : length(stns))
    {
      type = 1
      if(grepl("MFM", stns[t]))
        type = 0
      if(stns[t] == "WMS_2")
        type = 2
      pathmon1 = paste(pathmon,stns[t],sep="/")
      days = dir(pathmon1)
      path2Gmon1 = paste(path2Gmon,stns[t],sep = "/")
      if(!file.exists(path2Gmon1))
      {	
        dir.create(path2Gmon1)
      }
      
      
      if(length(days)>0)
      {
        for(z in 1 : length(days))
        {
          if(ENDCALL == 1)
            break
          if((z==length(days)) && (y == length(months)) && (x ==length(years)))
            next
          print(days[z])
          pathfinal = paste(pathmon1,days[z],sep = "/")
          path2Gfinal = paste(path2Gmon1,days[z],sep="/")
          if(RESETHISTORICAL)
          {
            secondGenData(pathfinal,path2Gfinal,type)
          }
          if(!dunmun){
            daysAlive = daysAlive+1
          }
          if(days[z] == stopDate)
          {
            ENDCALL = 1
            print('Hit end call')
            next
          }
        }
      }
      dunmun = 1
      if(ENDCALL == 1)
        break
    }
    if(ENDCALL == 1)
      break
  }
  if(ENDCALL == 1)
    break
}

print('Backlog done')

prevx = x
prevy = y
prevz = z
repeats = 0
SENDMAILTRIGGER = 0
newlen=1
while(1)
{
  recipients = c('operationsWestIN@cleantechsolar.com','om-it-digest@cleantechsolar.com')
  recordTimeMaster("IN-069L","Bot")
  years = dir(path)
  noyrs = length(years)
  path2Gfinalall = vector('list')
  for(x in prevx : noyrs)
  {
    pathyr = paste(path,years[x],sep="/")
    path2Gyr = paste(path2G,years[x],sep="/")
    checkdir(path2Gyr)
    mons = dir(pathyr)
    nomons = length(mons)
    startmn = prevy
    endmn = nomons
    if(startmn>endmn)
    {
      startmn = 1
      prevx = x-1
      prevz = 1
    }
    for(y in startmn:endmn)
    {
      pathmon = paste(pathyr,mons[y],sep="/")
      path2Gmon = paste(path2Gyr,mons[y],sep="/")
      checkdir(path2Gmon)
      stns = dir(pathmon)
      if(length(stns) < 25)
      {
        print('Station sync issue.. sleeping for an hour')
        Sys.sleep(3600) # Sync issue, meter data comes after MFM and WMS
        stns = dir(pathmon)
      }
      stns = stns[reorderStnPaths]
      for(t in 1 : length(stns))
      {
        newlen = (y-startmn+1) + (x-prevx)
        print(paste('Reset newlen to',newlen))
        type = 1
        if(grepl("MFM",stns[t]))
          type = 0
        if(stns[t] == "WMS_2")
          type = 2
        pathmon1 = paste(pathmon,stns[t],sep="/")
        days = dir(pathmon1)
        path2Gmon1 = paste(path2Gmon,stns[t],sep = "/")
        chkcopydays = days[grepl('Copy',days)]
        if(!file.exists(path2Gmon1))
        {
          dir.create(path2Gmon1)
        }
        if(length(chkcopydays) > 0)
        {
          print('Copy file found they are')
          print(chkcopydays)
          idxflse = match(chkcopydays,days)
          print(paste('idx matches are'),idxflse)
          for(innerin in 1 : length(idxflse))
          {
            command = paste("rm '",pathmon,"/",days[idxflse[innerin]],"'",sep="")
            print(paste('Calling command',command))
            system(command)
          }
          days = days[-idxflse]
        }
        days = days[complete.cases(days)]
        if(t==1)
          referenceDays <<- extractDaysOnly(days)
        else
          days = analyseDays(days,referenceDays)
        nodays = length(days)
        if(y > startmn)
        {
          z = prevz = 1
        }
        if(nodays > 0)
        {
          startz = prevz
          if(startz > nodays)
            startz = nodays
          for(z in startz : nodays)
          {
            if(t == 1)
            {
              path2Gfinalall[[newlen]] = vector('list')
            }
            if(is.na(days[z]))
              next
            pathdays = paste(pathmon1,days[z],sep = "/")
            path2Gfinal = paste(path2Gmon1,days[z],sep="/")
            secondGenData(pathdays,path2Gfinal,type)
            if((z == nodays) && (y == endmn) && (x == noyrs))
            {
              if(!repeats)
              {
                print('No new data')
                repeats = 1
              }
              next
            }
            if(is.na(days[z]))
            {
              print('Day was NA, do next in loop')
              next
            }
            SENDMAILTRIGGER <<- 1
            if(t == 1)
            {
              splitstr =unlist(strsplit(days[z],"-"))
              daysSend = paste(splitstr[4],splitstr[5],splitstr[6],sep="-")
              print(paste("Sending",daysSend))
              if(performBackWalkCheck(daysSend) == 0)
              {
                print("Sync issue, sleeping for an 1 hour")
                Sys.sleep(3600) # Sync issue -- meter data comes a little later than
                # MFM and WMS
              }
            }
            repeats = 0
            print(paste('New data, calculating digests',days[z]))
            path2Gfinalall[[newlen]][t] = paste(path2Gmon1,days[z],sep="/")
            newlen = newlen + 1
            print(paste('Incremented newlen by 1 to',newlen))
          }
        }
      }
    }
  }
  if(SENDMAILTRIGGER)
  {
    print('Sending mail')
    print(paste('newlen is ',newlen))
    for(lenPaths in 1 : (newlen - 1))
    {
      if(lenPaths < 1)
        next
      print(unlist(path2Gfinalall[[lenPaths]]))
      pathsend = unlist(path2Gfinalall[[lenPaths]])
      daysAlive = daysAlive+1
      sendMail(pathsend)
    }
    SENDMAILTRIGGER <<- 0
    newlen = 1
    print(paste('Reset newlen to',newlen))
    
  }
  
  prevx = x
  prevy = y
  prevz = z
  Sys.sleep(300)
}
print(paste('Exited for some reason x y z values are'),x,y,z)
sink()