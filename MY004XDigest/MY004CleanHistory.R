rm(list = ls())
errHandle = file('/home/admin/Logs/LogsMY004XHistory.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

require('mailR')
source('/home/admin/CODE/MY004XDigest/FTPMY004Dump.R')
source('/home/admin/CODE/Misc/memManage.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
FIREERRATA = c(1)
LTCUTOFF = 0.001
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

timetomins = function(x)
{
  hrs = unlist(strsplit(x,":"))
  mins = as.numeric(hrs[2])
  hrs = as.numeric(hrs[1]) * 60
  return((((hrs + mins))+1))
}
NACOUNTER = FAULTCOUNTER = c(0)
THRESHNA=THRESHFAULT=20

checkErrata = function(row,ts)
{
  if(ts < 540 || ts > 1020)
	{return()}
	if(FIREERRATA == 0)
	{
	  print(paste('Errata mail already sent so no more mails'))
		return()
	}
	{
	if(!is.finite(as.numeric(row[2])))
	{
		NACOUNTER<<-NACOUNTER+1
		if(NACOUNTER<THRESHNA)
		{
			print(paste('Incremented NA values for mt but its still less than thresh'))
			return()
		}
	}
	else if((as.numeric(row[2]) > LTCUTOFF))
	{
		NACOUNTER<<-0
		FAULTCOUNTER<<-0
	}
	else
	{
		FAULTCOUNTER<<-FAULTCOUNTER+1
		if(FAULTCOUNTER<THRESHFAULT)
		{
			print(paste('Incremented fault values for but its still less than thresh'))
			return()
		}
	}
	}
	if((!(is.finite(as.numeric(row[2])))) || (as.numeric(row[2]) < LTCUTOFF))
	{
		FIREERRATA <<- 0
		subj = paste('MY-004X down')
		body=""
		if(NACOUNTER>=THRESHNA)
			body = paste(body,"Error description: Ten consecutive NA readings detected for Pac\n",sep="")
		if(FAULTCOUNTER>=THRESHFAULT)
			body = paste(body,"Error description: Ten consecutive readings below threshold for Pac\n",sep="")
		body = paste(body,'\nTimestamp: ',as.character(row[1]),sep="")
		body = paste(body,'\n\nReal Power Tot kW reading: ',as.character(row[2]),sep="")
		send.mail(from = 'operations@cleantechsolar.com',
							to = getRecipients("MY-004X","a"),
							subject = subj,
							body = body,
							smtp = list(host.name = "smtp.office365.com", port = 587, user.name = 'shravan.karthik@cleantechsolar.com',passwd = 'CTS&*(789', tls = TRUE),
							authenticate = TRUE,
							send = TRUE,
							debug = F)
		print(paste('Errata mail sent meter '))
		msgbody = paste(subj,"\n Timestamp:",as.character(row[1]),
		                "\n Real Pow Tot kW reading:",as.character(row[2]))
		if(NACOUNTER>=THRESHNA)
			msgbody = paste(msgbody,"\nTen consecutive NA readings")
		if(FAULTCOUNTER>=THRESHFAULT)
			body = paste(body,"\nTen consecutive readings below threshold\n",sep="")
		command = paste('python /home/admin/CODE/Send_mail/twilio_alert.py',' "',msgbody,'"',' "MY-004X"',sep = "")    ########## CHANGE PYTHON SCRIPT TO REFLECT TRUE OWNERS AS ARUN IS REMOVED
		system(command)
		print('Twilio message fired')
		recordTimeMaster("MY-004X","TwilioAlert",as.character(row[1]))
		NACOUNTER<<-0
		FAULTCOUNTER<<-0
	}
}

stitchFile = function(path,days,pathwrite,erratacheck)
{
print('Inside stitchFile')
x = 1
t = try(read.csv(paste(path,days[x],sep="/"),header = T, skip=6,stringsAsFactors=F),silent = T)
  if(length(t)==1)
        {
                print(paste("Skipping",days[x],"Due to err in file"))
                return()
        }
  dataread = t
        if(nrow(dataread) < 1)
                return()
  dataread = dataread[,-c(1,2)]
	colnames(dataread)[1] = "Tm"
  currcolnames = colnames(dataread)
  #temp = unlist(strsplit(days[x]," "))
  #      temp = unlist(strsplit(temp[2],"_"))
  #temp = as.numeric(temp[1])a
	tmstmps = as.character(dataread[,1])
	tmstmps = unlist(strsplit(tmstmps," "))
	seqa = seq(from =1 ,to=length(tmstmps),by=2)
	tmstmps2 = tmstmps[seqa]
	tmstmps = tmstmps[(seqa+1)]
	tmstmps3 = unlist(strsplit(tmstmps2,"-"))
	seqb = seq(from=1,to=length(tmstmps3),by=3)
	tmstmps3 = paste(tmstmps3[(seqb+2)],tmstmps3[(seqb)],tmstmps3[(seqb+1)],sep="-")
	tmstmps = paste(tmstmps3,tmstmps)
	dataread[,1]=as.character(tmstmps)
	temp = substr(days[x],1,3)
	temp = 1
	mtno = 1
	rowtempuse = rowtemp
	indexpass = 7
	colnamesuse = colnames
	
	idxvals = match(currcolnames,colnamesuse)
	idxvals = idxvals[complete.cases(idxvals)]

  for(y in 1 : nrow(dataread))
  {
    idxts = timetomins(substr(as.character(dataread[y,1]),12,16))
    if(!is.finite(idxts))
    {
      print(paste('Skipping row',y,'as incorrect timestamp'))
      next
    }
    yr = substr(as.character(dataread[y,1]),1,4)
    pathwriteyr = paste(pathwrite,yr,sep="/")
    checkdir(pathwriteyr)
    mon = substr(as.character(dataread[y,1]),1,7)
    pathwritemon = paste(pathwriteyr,mon,sep="/")
    checkdir(pathwritemon)
		pathwritefinal = pathwritemon
    checkdir(pathwritefinal)
    day = substr(as.character(dataread[y,1]),1,10)
    filename = paste(stnname," ",day,".txt",sep="")
    pathtowrite = paste(pathwritefinal,filename,sep="/")
    rowtemp2 = rowtempuse
		print(paste('index is',idxvals))
		print('------------------------------')
		idxvals2 = idxvals
    rowtemp2[idxvals] = dataread[y,idxvals2]
		print(paste('Writing files',pathtowrite))
		{
   	 	if(!file.exists(pathtowrite))
    	{
      	df = data.frame(rowtemp2,stringsAsFactors = F)
      	colnames(df) = colnamesuse
      	if(idxts != 1)
        	{
          	df[idxts,] = rowtemp2
          	df[1,] = rowtempuse
        	}
				FIREERRATA <<- 1
				NACOUNTER <<- 0
				FAULTCOUNTER <<-0
    	}
    	else
    	{
      	df = read.table(pathtowrite,header =T,sep = "\t",stringsAsFactors=F)
      	df[idxts,idxvals] = rowtemp2[idxvals]
    	}
  	}
		if(erratacheck != 0)
		{
			passrow2 = df[idxts,indexpass]
			pass = c(as.character(df[idxts,1]),as.character(passrow2))
			checkErrata(pass,idxts,mtno)
		}
		df = df[complete.cases(df[,1]),]
		tdx = as.POSIXct(strptime(as.character(df[,1]), "%Y-%m-%d %H:%M:%S"))
		df = df[order(tdx),]
  	write.table(df,file = pathtowrite,row.names = F,col.names = T,sep = "\t",append = F)
  }
	recordTimeMaster("MY-004X","Gen1",as.character(df[nrow(df),1]))
}

path = '/home/admin/Data/Episcript-CEC/EGX MY004X Dump'
pathwrite = '/home/admin/Dropbox/Gen 1 Data/[MY-004X]'
pathdatelastread = '/home/admin/Start'
checkdir(pathdatelastread)
pathdatelastread = paste(pathdatelastread,'MY004X.txt',sep="/")
days = dir(path)
days = days[grepl("csv",days)]
lastrecordeddate = c('')
idxtostart = c(1)

{
	if(!file.exists(pathdatelastread))
	{
		print('Last date read file not found so cleansing system from start')
		system('rm -R "/home/admin/Dropbox/Gen 1 Data/[MY-004X]"')
	}
	else
	{
		lastrecordeddate = readLines(pathdatelastread)
		lastrecordeddate = c(lastrecordeddate)
		idxtostart = match(lastrecordeddate,days)
		print(paste('Date read is',lastrecordeddate,'and idxtostart is',idxtostart))
	}
}

checkdir(pathwrite)
colnames = colnames(read.csv(paste(path,days[idxtostart],sep="/"),header = T, skip=6,stringsAsFactors=T))
colnames = colnames[-c(1,2)]

colnames[1] = "Tm"

rowtemp = rep(NA,(length(colnames)))

x=1
stnname =  "[MY-004X]"

{
	if(idxtostart < length(days))
	{
		print(paste('idxtostart[1] is',idxtostart,'while length of days is',length(days)))
		for(x in idxtostart : length(days))
		{
 		 stitchFile(path,days[x],pathwrite,0)
		}
		lastrecordeddate = as.character(days[x])
		print('Meter 1 DONE')
	}
	else if(!(idxtostart < length(days))) 
	{
		print('No historical backlog')
	}
}

print('_________________________________________')
print('Historical Calculations done')
print('_________________________________________')
reprint = 1
print(lastrecordeddate)
write(lastrecordeddate,pathdatelastread)
print('File Written')
ERRATAFIX=0

while(1)
{
  print('Checking FTP for new data')
  filefetch = dumpftp(days,path)
	if(filefetch == 0)
	{
		Sys.sleep(600)
		next
	}
	print('FTP Check complete')
	daysnew = dir(path)
	daysnew = daysnew[grepl("csv",daysnew)]
	if(length(daysnew) == length(days))
	{
	  if(reprint == 1)
		{
			reprint = 0
			print('Waiting for new file dump')
		}
		Sys.sleep(600)
		next
	}
	reprint = 1
	match = match(days,daysnew)
	days = daysnew
	daysnew = daysnew[-match]
	for(x in 1 : length(daysnew))
	{
	  print(paste('Processing',daysnew[x]))
		stitchFile(path,daysnew[x],pathwrite,ERRATAFIX)
		print(paste('Done',daysnew[x]))
	}
	
	write(c(as.character(daysnew[length(daysnew)])),pathdatelastread)
	lastrecordeddate = as.character(daysnew[length(daysnew)])
gc()
ERRATAFIX=0 #for now once GSI reading is fine change this to 1
}

print('Out of loop')
sink()
