rm(list = ls())
TIMESTAMPSALARM = NULL
#TIMESTAMPSALARM2 = NULL
METERCALLED = 0
ltcutoff = .001
CABLOSSTOPRINT = 0
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
	seq1 = seq(from = 1,to = length(hr),by =2)
	seq2 = seq(from = 2,to = length(hr),by =2)
  min = as.numeric(hr[seq2]) 
  hr = as.numeric(hr[seq1]) * 60
  return((hr + min + 1))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
secondGenData = function(filepath,writefilepath)
{
  {
		if(METERCALLED == 1){
	 		TIMESTAMPSALARM <<- NULL}
#		else if(METERCALLED == 2){
#			TIMESTAMPSALARM2 <<- NULL}
	}
  dataread = read.table(filepath,header = T,sep = "\t",stringsAsFactors=F)
	GSITOT = TMODMEAN=TMODMEANSH=lasttime=lastread=Eac2=Eac1=RATIO=DA=totrowsmissing=date=YLD1=YLD2=PR1=PR2=NA
	if(length(as.numeric(dataread[complete.cases(as.numeric(dataread[,48])),48])))
	GSITOT = round(sum(as.numeric(dataread[complete.cases(as.numeric(dataread[,48])),48]))/60000,2)
	if(length(as.numeric(dataread[complete.cases(as.numeric(dataread[,49])),49])))
	TMODMEAN = round(mean(as.numeric(dataread[complete.cases(as.numeric(dataread[,49])),49])),1)
	dataread2 = dataread
	dataread = dataread2[complete.cases(as.numeric(dataread2[,39])),]
	if(nrow(dataread))
	{
	lasttime = as.character(dataread[nrow(dataread),1])
	lastread = as.character(round(as.numeric(dataread[nrow(dataread),39])/1000,3))
	Eac2 = format(round((as.numeric(dataread[nrow(dataread),39]) - as.numeric(dataread[1,39]))/1000,1),nsmall=1)
	}
	YLD2 = format(round(as.numeric(Eac2)/503.5,2),nsmall=2)
	dataread = dataread2[complete.cases(as.numeric(dataread2[,15])),]
	dataread = dataread[as.numeric(dataread[,15]) > 0,]
	Eac1 = 0
	if(nrow(dataread) > 1)
	{
   	Eac1 = format(round(sum(as.numeric(dataread[,15]))/60000,1),nsmall=1)
	}
	tempData = as.numeric(dataread[complete.cases(as.numeric(dataread[,50])),50])
	ActE = NA
	if(length(tempData)>1)
	{
		ActE = (tempData[length(tempData)] - tempData[1])/1000
		#ActE = tempData[length(tempData)]/1000
	}
	YLD1 = format(round(as.numeric(Eac1)/503.5,2),nsmall=2)
	RATIO = round(as.numeric(Eac1)*100/as.numeric(Eac2),1)
	dataread = dataread2
  DA = format(round(nrow(dataread)/14.4,1),nsmall=1)
  tdx = timetomins(substr(dataread[,1],12,16))
  dataread2 = dataread[tdx > 540,]
  tdx = tdx[tdx > 540]
  dataread2 = dataread2[tdx < 1020,]
	if(length(as.numeric(dataread2[complete.cases(as.numeric(dataread2[,49])),49])))
	TMODMEANSH = round(mean(as.numeric(dataread2[complete.cases(as.numeric(dataread2[,49])),49])),1)
	dataread2 = dataread2[complete.cases(as.numeric(dataread2[,15])),]
  missingfactor = 480 - nrow(dataread2)
  dataread2 = dataread2[as.numeric(dataread2[,15]) < ltcutoff,]
	if(length(dataread2[,1]) > 0)
	{
		#{
			if(METERCALLED == 1){
				TIMESTAMPSALARM <<- as.character(dataread2[,1])}
			#else if(METERCALLED == 2){
		 	# TIMESTAMPSALARM2 <<- as.character(dataread2[,1])}
		#}
	}
  totrowsmissing = format(round((missingfactor + nrow(dataread2))/4.8,1),nsmall=1)
	if(nrow(dataread))
	date = as.character(dataread[1,1],1,10)
	PR1=format(round(as.numeric(YLD1)*100/GSITOT,1),nsmall=1)
	PR2=format(round(as.numeric(YLD2)*100/GSITOT,1),nsmall=1)
	{
	  if(METERCALLED == 1)
	  {
      df = data.frame(Date = date, SolarPowerMeth1 = as.numeric(Eac1),SolarPowerMeth2 = as.numeric(Eac2),
                  Ratio=RATIO,DA = DA,Downtime = totrowsmissing,
									LastTime = lasttime, LastRead = lastread,GSITot = GSITOT,TModMean=TMODMEAN,TModMeanSH=TMODMEANSH,
									Yield1=YLD1,Yield2=YLD2,PR1=PR1,PR2=PR2,ActiveEnergy=ActE,stringsAsFactors = F)
    } 
	  #else if(METERCALLED == 2)
	  #{
     # df = data.frame(Date = substr(as.character(dataread[1,1]),1,10), CokeEnergyMeth1 = as.numeric(Eac1),CokeEnergyMeth2= as.numeric(Eac2),
     #             Ratio=RATIO,DA = DA,Downtime = totrowsmissing,
		#							LastTime = lasttime, LastRead = lastread, stringsAsFactors = F)
	  #}
	}
	write.table(df,file = writefilepath,row.names = F,col.names = T,sep="\t",append = F)
  return(df)
}

thirdGenData = function(filepathm2,writefilepath)
{
  #dataread2 =read.table(filepathm1,header = T,sep="\t") #its correct dont change
  dataread1 = read.table(filepathm2,header = T,sep = "\t") #its correct dont change
	dt = as.character(dataread1[,6])
	da = as.character(dataread1[,7])
	#EacCokeMeth1 = as.numeric(dataread2[,2])
	#EacCokeMeth2 = as.numeric(dataread2[,3])
	SolarEnergyMeth1 = as.numeric(dataread1[,2])
	SolarEnergyMeth2 = as.numeric(dataread1[,3])
  #RatioCoke = as.numeric(dataread2[,4])
  RatioSolar = as.numeric(dataread1[,4])
	#CABLOSS= round((EacCokeMeth2)/SolarEnergyMeth2,3)
	#CABLOSS = abs(1 - CABLOSS)*100
	#CABLOSSTOPRINT <<- CABLOSS
	ltSol = as.character(dataread1[,7])
	lrSol = as.character(dataread1[,8])
	#ltCoke = as.character(dataread2[,7])
	#lrCoke = as.character(dataread2[,8])
	GSITOT=as.character(dataread1[,9])
	TMODMean = as.character(dataread1[,10])
	TMODMEANSH=as.character(dataread1[,11])
  df = data.frame(Date = substr(as.character(dataread1[1,1]),1,10), DA= da,Downtime = dt,
	#EacCokeMeth1 = EacCokeMeth1,
	#EacCokeMeth2 = EacCokeMeth2,
	SolarEnergyMeth1=SolarEnergyMeth1,SolarEnergyMeth2=SolarEnergyMeth2,
	#RatioCoke=RatioCoke,
	RatioSolar=RatioSolar,
	#CableLoss=CABLOSS,
	LastTimeSol=ltSol,LastReadSol=lrSol,
	#LastTimeCoke=ltCoke,LastReadCoke=lrCoke,
	GSITot=GSITOT,TModMean=TMODMean,TModMeanSH=TMODMEANSH,ActiveEnergy=as.numeric(dataread1[,16]),stringsAsFactors=F)
  {
    if(file.exists(writefilepath))
    {
      write.table(df,file = writefilepath,row.names = F,col.names = F,sep = "\t",append = T)
    }
    else
    {
      write.table(df,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
    }
  }
}

