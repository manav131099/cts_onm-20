rm(list=ls(all =TRUE))
#####         712 is IN 015 from Chenxi's code
pathRead <- "~/intern/Data/[712]"
pathWrite <- "C:/Users/talki/Desktop/cec intern/results/712/712]_summary.txt"
pathWrite2 <- "C:/Users/talki/Desktop/cec intern/results/712/[712]_summary.CSV"

setwd(pathRead)
filelist <- dir(pattern = ".txt", recursive= TRUE)
timemin <- format(as.POSIXct("2016-07-18 06:59:59"), format="%H:%M:%S")
timemax <- format(as.POSIXct("2017-10-02 17:00:00"), format="%H:%M:%S")

stationLimit <- 1814.4
nameofStation <- "IN-015A"

col0 <- c()
col1 <- c()
col2 <- c()
col3 <- c()
col4 <- c()
col5 <- c()
col6 <- c()
col7 <- c()
col0[10^6] = col1[10^6] = col2[10^6] = col3[10^6] = col4[10^6] = col5[10^6] = col6[10^6] =col7[10^6] =0
index <- 1

for (i in filelist){
  data <- NULL
  temp <- read.table(i,header = T, sep = "\t")
  date <- substr(paste(temp[1,1]),1,10)
  col0[index] <- nameofStation
  col1[index] <- date
  
  #data availability
  pts <- length(temp[,1])
  col3[index] <- pts
  
  #pac
  condition <- format(as.POSIXct(temp[,1]), format="%H:%M:%S") > timemin &
    format(as.POSIXct(temp[,1]), format="%H:%M:%S") <timemax
  pacTemp <- temp[condition,]
  pacTemp <- pacTemp[complete.cases(pacTemp[,70]),]
  pac <- as.numeric(pacTemp[,70])*(-1)
  pac <- pac[pac!=0]
  numer <- length(pac[pac>0.1])
  pacTemp2 <- pacTemp[pac<0.1,]
  pacTemp2 <- pacTemp2[pacTemp2[,3]>20,]
  denom <- length(pacTemp2[,1])+numer
  flagRate <- numer/denom
  col4[index] <- flagRate*100
  
  #wpac
  gsi <- pacTemp[,3]
  flag <- as.numeric(pac<0.1 & gsi > 20)
  wgsi <- gsi/sum(gsi)
  downloss <- wgsi * flag * 100
  if (length(downloss)>0){
    col5[index] <- 100 - sum(downloss)
  }
  else{
    col5[index] <- NA
  }
  
  #pr
  gsi <- temp[,3]
  eac <- temp[,86][complete.cases(temp[,86])]
  eac <- eac[eac < 10^8]
  diff <- eac[length(eac)]- eac[1]
  pr <- diff/stationLimit/sum(gsi)*60000
  if (length(pr)==0){
    col6[index] = NA
  }
  else{
    col6[index] <- pr*100
  }
  #transloss
  powerlv <- as.numeric(temp[,23])
  gsi <- as.numeric(temp[,3])
  powerlv[gsi<20] <- NA
  powerhv <- as.numeric(temp[,70])
  powerhv[gsi<20] <- NA
  trans = (powerlv-powerhv)/powerlv
  trans[abs(trans) == Inf] <- NA
  col7[index] <- mean(trans[!is.na(trans)])*100
  
  
  print(paste(i, " done"))
  index <- index + 1
}

col0 <- col0[1:(index-1)]
col1 <- col1[1:(index-1)]
col2 <- col2[1:(index-1)]
col3 <- col3[1:(index-1)]
col4 <- col4[1:(index-1)]
col5 <- col5[1:(index-1)]
col6 <- col6[1:(index-1)]
col7 <- col7[1:(index-1)]
col2 <- col3/max(col3)*100
col2[is.na(col2)] <- NA
col3[is.na(col3)] <- NA
col4[is.na(col4)] <- NA
col5[is.na(col5)] <- NA
col6[is.na(col6)] <- NA
col7[is.na(col7)] <- NA


result <- cbind(col0,col1,col2,col3,col4,col5,col6,col7)
colnames(result) <- c("Meter Reference","Date","Data Availability [%]",
                      "No. of Points", "Uptime [%]","Weighted Uptime [%]",
                      "Performance Ratio [%]", "Transloss")
result[,3] <- round(as.numeric(result[,3]),1)
result[,5] <- round(as.numeric(result[,5]),1)
result[,6] <- round(as.numeric(result[,6]),1)
result[,7] <- round(as.numeric(result[,7]),1)

rownames(result) <- NULL
result <- data.frame(result)
result <- result[-length(result[,1]),]
dataWrite <- result
write.table(result,na = "",pathWrite,row.names = FALSE,sep ="\t")
write.csv(result,pathWrite2, na= "", row.names = FALSE)

